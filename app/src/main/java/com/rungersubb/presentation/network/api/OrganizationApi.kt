package com.rungersubb.presentation.network.api

import com.rungersubb.presentation.model.response.CompactOrganizationResponse
import com.rungersubb.presentation.network.api.common.RetrofitApi
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

interface OrganizationApi: RetrofitApi {

    //Usable without auth token
    @POST("api/organization/list-compact/get")
    suspend fun postOrganizationListCompactGet(@Body body: RequestBody): List<CompactOrganizationResponse>
}