package com.rungersubb.presentation.utils.constants

object AnimationExtra {
    const val DURATION_FROM_SPLASH_TO_SIGN_IN = 350L
    const val DURATION_LONG = 500L

    const val ALPHA_VISIBLE = 1F
    const val ALPHA_INVISIBLE = 0F
}