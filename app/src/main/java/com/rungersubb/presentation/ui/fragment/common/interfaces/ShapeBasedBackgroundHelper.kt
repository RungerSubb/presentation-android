package com.rungersubb.presentation.ui.fragment.common.interfaces

import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.rungersubb.presentation.R
import com.rungersubb.presentation.utils.tools.BitmapTool

interface ShapeBasedBackgroundHelper {
    fun createBackButtonBackground(background: ImageView) =
        background.setImageBitmap(BitmapTool.backButtonBackground)
}