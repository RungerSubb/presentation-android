package com.rungersubb.presentation.utils.tools.keyboard

import android.app.Activity

interface KeyboardBehaviorHandler{
    fun registerKeyboardBehaviorHandler(activity: Activity): Unit =
        KeyboardBehaviorEvent.registerEventListener(activity, this)

    fun onShowKeyboard(keyboardHeight: Int)
    fun onHideKeyboard()
}