package com.rungersubb.presentation.model.ui

import com.google.gson.annotations.SerializedName
import com.rungersubb.presentation.model.common.validation.ValidatableUserModel
import com.rungersubb.presentation.model.enums.UserType
import com.rungersubb.presentation.model.request.UserRequest
import com.rungersubb.presentation.utils.constants.CommonConstants
import org.joda.time.DateTime
import java.io.Serializable

data class UserDataInputModel(
    @SerializedName("first_name")
    override val firstName: String,

    @SerializedName("last_name")
    override val lastName: String,

    @SerializedName("email")
    override val email: String,

    @SerializedName("password")
    override val password: String,

    @SerializedName("birth_date")
    override val birthDate: DateTime?
): Serializable, ValidatableUserModel {

    companion object{
        fun fromRequest(request: UserRequest?): UserDataInputModel? =
            if(request != null)
                UserDataInputModel(
                    request.firstName,
                    request.lastName,
                    request.email,
                    request.password,
                    DateTime(request.birthDate)
                )
            else null
    }

    fun request(userType: String = UserType.USER.value): UserRequest =
        UserRequest(
            firstName,
            lastName,
            email,
            password,
            birthDate?.millis ?: CommonConstants.DEFAULT_BIRTH_DATE,
            userType
        )
}