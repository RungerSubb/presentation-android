package com.rungersubb.presentation.model.common.validation

import org.joda.time.DateTime

interface ValidatableUserModel: ValidatableSignInModel {
    val firstName: String
    val lastName: String
    val birthDate: DateTime?

    fun isFirstNameValid(): Boolean =
        firstName.isNotEmpty() && firstName.isNotBlank()

    fun isLastNameValid(): Boolean =
        lastName.isNotEmpty() && lastName.isNotBlank()

    fun isBirthDateValid(): Boolean =
        birthDate != null && birthDate?.isBeforeNow ?: false

    override fun isModelValid(): Boolean =
        isFirstNameValid() &&
        isLastNameValid() &&
        isEmailValid() &&
        isPasswordValid() &&
        isBirthDateValid()
}
