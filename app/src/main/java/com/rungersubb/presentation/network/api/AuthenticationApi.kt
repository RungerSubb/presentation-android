package com.rungersubb.presentation.network.api

import com.rungersubb.presentation.model.request.SignInRequest
import com.rungersubb.presentation.model.response.AuthenticationResponse
import com.rungersubb.presentation.network.api.common.RetrofitApi
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST


interface AuthenticationApi: RetrofitApi {

    //Usable without auth token
    @POST("/api/auth/sign-in")
    suspend fun postAuthSignIn(@Body body: RequestBody): AuthenticationResponse

    //Usable without auth token
    @POST("/api/auth/sign-up")
    suspend fun postAuthSignUp(@Body body: RequestBody): AuthenticationResponse
}