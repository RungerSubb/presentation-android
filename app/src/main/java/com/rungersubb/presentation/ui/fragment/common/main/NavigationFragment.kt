package com.rungersubb.presentation.ui.fragment.common.main

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.rungersubb.presentation.utils.extension.safeFromIllegalArgument

abstract class NavigationFragment<BINDING: ViewBinding, VIEW_MODEL: ViewModel>:
    ViewModelFragment<BINDING, VIEW_MODEL>() {

    companion object{ const val NAVIGATION_EMPTY_DESTINATION = -1 }

    private val parentNavController: NavController by lazy { findNavController() }
    protected val parentDestination: Int
        get() = parentNavController.currentDestination?.id ?: NAVIGATION_EMPTY_DESTINATION


    protected fun navigate(navigationWay: Int, arguments: Bundle = bundleOf()){
        safeFromIllegalArgument { parentNavController.navigate(navigationWay, arguments) }
    }
}