package com.rungersubb.presentation.utils.constants

object ViewPagerPageNumber {
    const val CREATE_ORGANIZATION = 0
    const val SELECT_ORGANIZATION = 1
}