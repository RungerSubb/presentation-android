package com.rungersubb.presentation.mvvm.fragment.login.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rungersubb.presentation.application.SharedPreferencesProvider
import com.rungersubb.presentation.model.common.validation.ValidatableSignInModel
import com.rungersubb.presentation.model.request.SignInRequest
import com.rungersubb.presentation.model.response.AuthenticationResponse
import com.rungersubb.presentation.mvvm.common.LoginHelper
import com.rungersubb.presentation.mvvm.common.network.AuthenticationRepositoryProvider
import com.rungersubb.presentation.utils.extension.launchSafe
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class SignInViewModel(private val sharedPreferencesProvider: SharedPreferencesProvider):
    ViewModel(),
    ValidatableSignInModel,
    LoginHelper,
    AuthenticationRepositoryProvider {

    override val email: String
        get() = emailFlow.value ?: String()
    override val password: String
        get() = passwordFlow.value ?: String()

    //region Flow
    private val emailMutableFlow: MutableStateFlow<String?> =
        MutableStateFlow(null)
    val emailFlow: StateFlow<String?>
        get() = emailMutableFlow.asStateFlow()

    private val passwordMutableFlow: MutableStateFlow<String?> =
        MutableStateFlow(null)
    val passwordFlow: StateFlow<String?>
        get() = passwordMutableFlow.asStateFlow()

    private val authenticationMutableFlow: MutableStateFlow<AuthenticationResponse?> =
        MutableStateFlow(null)
    val authenticationFlow: StateFlow<AuthenticationResponse?>
        get() = authenticationMutableFlow.asStateFlow()
    //endregion

    //region Flow setters
    fun setEmail(email: String){
        emailMutableFlow.value = email
        sharedPreferencesProvider.saveEmail(email)
    }

    fun setPassword(password: String){
        passwordMutableFlow.value = password
        sharedPreferencesProvider.savePassword(password)
    }
    //endregion

    //region LoginHelper
    override fun loadSavedInputs(): Unit =
        with(sharedPreferencesProvider){
            emailMutableFlow.value = getEmail()
            passwordMutableFlow.value = getPassword()
        }

    override fun clearData() {
        emailMutableFlow.value = null
        passwordMutableFlow.value = null

        with(sharedPreferencesProvider){
            saveEmail(null)
            savePassword(null)
        }
    }

    override fun isInputCompleted(): Boolean =
        password.isNotEmpty() && email.isNotEmpty()

    override fun isNoDataEntered(): Boolean =
        password.isEmpty() && email.isEmpty()

    //endregion

    //region AuthenticationRepositoryProvider
    override fun authenticate(){
        viewModelScope.launchSafe(
            {
                val request = SignInRequest(email, password)
                val response = authenticationRepository.requestSignIn(request)
                authenticationMutableFlow.value = response
            },
            { authenticationMutableFlow.value = AuthenticationResponse() }
        )
    }
    //endregion
}