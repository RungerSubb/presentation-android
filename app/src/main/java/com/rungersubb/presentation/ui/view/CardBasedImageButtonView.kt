package com.rungersubb.presentation.ui.view

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import androidx.core.content.ContextCompat
import androidx.core.view.updateLayoutParams
import com.google.android.material.card.MaterialCardView
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.ViewCardBasedImageButtonBinding
import com.rungersubb.presentation.databinding.ViewCardBasedTextButtonBinding
import com.rungersubb.presentation.ui.view.common.CoreView

class CardBasedImageButtonView(context: Context, attributeSet: AttributeSet):
    MaterialCardView(context, attributeSet), CoreView<ViewCardBasedImageButtonBinding> {

    override val binding: ViewCardBasedImageButtonBinding =
        ViewCardBasedImageButtonBinding.inflate(
            LayoutInflater.from(context),
            this,
            true
        )

    init {
        val attributes = context.obtainStyledAttributes(
            attributeSet,
            R.styleable.CardBasedImageButtonView
        )
        setupWithAttributes(attributes)
        attributes.recycle()
    }

    override fun setupWithAttributes(attributes: TypedArray): Unit =
        with(binding){
            attributes.apply {

                mainImageView.apply {
                    setImageResource(
                        getResourceId(
                            R.styleable.CardBasedImageButtonView_cbibv_image,
                            R.drawable.icon_back
                        )
                    )
                    setColorFilter(
                        getColor(
                            R.styleable.CardBasedImageButtonView_cbibv_imageColor,
                            context.getColor(R.color.white)
                        )
                    )

                    val horizontalPadding = getDimensionPixelSize(
                        R.styleable.CardBasedImageButtonView_cbibv_imageHorizontalPadding,
                        0
                    )
                    val verticalPadding = getDimensionPixelSize(
                        R.styleable.CardBasedImageButtonView_cbibv_imageVerticalPadding,
                        0
                    )

                    updateLayoutParams<LayoutParams> {
                        setMargins(
                            horizontalPadding,
                            verticalPadding,
                            horizontalPadding,
                            verticalPadding
                        )
                    }
                }
            }
        }

    fun setImageResource(resourceId: Int): Unit =
        binding.mainImageView.setImageResource(resourceId)
}