package com.rungersubb.presentation.model.ui

import com.rungersubb.presentation.model.enums.SignUpStepCondition

data class SignUpStepModel(
    val stringResource: Int,
    val condition: SignUpStepCondition
)
