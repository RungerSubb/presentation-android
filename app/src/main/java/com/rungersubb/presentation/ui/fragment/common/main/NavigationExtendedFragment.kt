package com.rungersubb.presentation.ui.fragment.common.main

import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.viewbinding.ViewBinding

abstract class NavigationExtendedFragment<BINDING: ViewBinding, VIEW_MODEL: ViewModel>:
    NavigationFragment<BINDING, VIEW_MODEL>(){

        protected abstract val childNavController: NavController

        protected val childDestination: Int
            get() = childNavController.currentDestination?.id ?: NAVIGATION_EMPTY_DESTINATION

}