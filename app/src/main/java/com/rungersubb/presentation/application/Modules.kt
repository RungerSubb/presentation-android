package com.rungersubb.presentation.application

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.rungersubb.presentation.mvvm.activity.MainViewModel
import com.rungersubb.presentation.mvvm.fragment.login.LoginNavigationViewModel
import com.rungersubb.presentation.mvvm.fragment.login.dialog.CreateOrSelectOrganizationViewModel
import com.rungersubb.presentation.mvvm.fragment.login.dialog.EnterAddressDataViewModel
import com.rungersubb.presentation.mvvm.fragment.login.dialog.EnterUserDataViewModel
import com.rungersubb.presentation.mvvm.fragment.login.screens.SignInViewModel
import com.rungersubb.presentation.mvvm.fragment.login.screens.SignUpViewModel
import com.rungersubb.presentation.mvvm.fragment.login.screens.SplashViewModel
import com.rungersubb.presentation.network.api.AuthenticationApi
import com.rungersubb.presentation.network.api.OrganizationApi
import com.rungersubb.presentation.network.repository.AuthenticationRepository
import com.rungersubb.presentation.network.repository.OrganizationRepository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit

private const val LOCAL_URL = "http://192.168.0.102:8080/"
private const val PROD_URL = "https://runger-subb.cf/"

private const val HEADER_AUTHORIZATION = "Authorization"
private const val HEADER_PLATFORM = "Platform-Key"

private const val PLATFORM_KEY = "9jc3WnQzdN2lHHOlNIYg"

private const val TIMEOUT_TIME = 30L

val applicationModule: Module = module {
    single { SharedPreferencesProvider(androidContext()) }

    //view models
    viewModel { MainViewModel() }

    //fragment
    viewModel { LoginNavigationViewModel() }
    viewModel { SplashViewModel() }
    viewModel { SignInViewModel(get()) }
    viewModel { SignUpViewModel(get()) }

    //dialog fragment
    viewModel { EnterUserDataViewModel() }
    viewModel { CreateOrSelectOrganizationViewModel() }
    viewModel { EnterAddressDataViewModel() }
}

val networkModule = module {
    factory { provideRetrofit(get()) }

    single { provideOrganizationRepository(get()) }
    single { provideAuthenticationRepository(get()) }
}

private fun provideRetrofit(sharedPreferencesProvider: SharedPreferencesProvider): Retrofit =
    Retrofit.Builder().apply {
        baseUrl(PROD_URL)
        client(OkHttpClient.Builder().apply {
            addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.HEADERS })
            addInterceptor { chain ->
                chain.proceed(
                    chain.request().newBuilder().apply {
                        addHeader(HEADER_PLATFORM, PLATFORM_KEY)
                    }.build()
                )
            }
            addInterceptor { chain ->
                chain.proceed(
                    chain.request().newBuilder().apply {
                        addHeader(
                            HEADER_AUTHORIZATION,
                            sharedPreferencesProvider.getToken() ?: String()
                        )
                    }.build()
                )
            }

            connectTimeout(TIMEOUT_TIME, TimeUnit.SECONDS)
            writeTimeout(TIMEOUT_TIME, TimeUnit.SECONDS)
            readTimeout(TIMEOUT_TIME, TimeUnit.SECONDS)
        }.build())
        addConverterFactory(GsonConverterFactory.create(
            GsonBuilder().apply {
                setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            }.create())
        )
    }.build()


//region repositories
private fun provideOrganizationRepository(retrofit: Retrofit): OrganizationRepository =
    OrganizationRepository(retrofit.create(OrganizationApi::class.java))

private fun provideAuthenticationRepository(retrofit: Retrofit): AuthenticationRepository =
    AuthenticationRepository(retrofit.create(AuthenticationApi::class.java))
//endregion