package com.rungersubb.presentation.ui.fragment.common.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

abstract class BindingFragment<BINDING: ViewBinding>: Fragment() {

    protected var binding: BINDING? = null

    protected abstract fun createBinding(inflater: LayoutInflater): BINDING

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = createBinding(inflater)
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    protected fun getColor(resourceId: Int): Int =
        ContextCompat.getColor(requireContext(), resourceId)

    protected fun getDimension(resourceId: Int): Float =
        requireContext().resources.getDimension(resourceId)
}