package com.rungersubb.presentation.ui.fragment.login.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.DialogFragmentCreateOrSelectOrganizationBinding
import com.rungersubb.presentation.model.request.OrganizationRequest
import com.rungersubb.presentation.mvvm.fragment.login.dialog.CreateOrSelectOrganizationViewModel
import com.rungersubb.presentation.ui.fragment.common.viewpager.ViewPagerContainerDialogFragment
import com.rungersubb.presentation.ui.fragment.common.viewpager.ViewPagerFragment
import com.rungersubb.presentation.ui.fragment.common.interfaces.ShapeBasedBackgroundHelper
import com.rungersubb.presentation.ui.fragment.common.interfaces.UiUpdater
import com.rungersubb.presentation.ui.fragment.login.viewpager.CreateOrganizationFragment
import com.rungersubb.presentation.ui.fragment.login.viewpager.SelectOrganizationFragment
import com.rungersubb.presentation.utils.constants.Arguments
import com.rungersubb.presentation.utils.constants.RequestKey
import com.rungersubb.presentation.utils.constants.ViewPagerPageNumber
import com.rungersubb.presentation.utils.extension.*
import com.rungersubb.presentation.utils.tools.keyboard.KeyboardBehaviorHandler
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel

class CreateOrSelectOrganizationDialogFragment private constructor():
    ViewPagerContainerDialogFragment<DialogFragmentCreateOrSelectOrganizationBinding, CreateOrSelectOrganizationViewModel>(),
    KeyboardBehaviorHandler,
    ShapeBasedBackgroundHelper,
    UiUpdater {

    companion object {
        private const val TAG = "DIALOG_FRAGMENT_CREATE_OR_SELECT_ORGANIZATION"

        private var instance: CreateOrSelectOrganizationDialogFragment? = null

        val isDisplaying: Boolean
            get() = instance != null

        private val isAbleToDisplay: Boolean
            get() = !isDisplaying && !EnterUserDataDialogFragment.isDisplaying && !EnterAddressDataDialogFragment.isDisplaying

        fun display(fragmentManager: FragmentManager, arguments: Bundle = bundleOf()) {
            if (isAbleToDisplay) {
                instance = CreateOrSelectOrganizationDialogFragment().apply {
                    this.arguments = arguments
                    show(fragmentManager, TAG)
                }
            }
        }
    }

    override val viewModel: CreateOrSelectOrganizationViewModel by viewModel()
    override val animationStyle: Int
        get() = R.style.SlideDialogFragmentAnimations
    override val requestKey: String
        get() = RequestKey.SELECT_OR_CREATE_ORGANIZATION_DIALOG_FRAGMENT

    //region ViewPagerContainerDialogFragment
    override fun createBinding(inflater: LayoutInflater): DialogFragmentCreateOrSelectOrganizationBinding =
        DialogFragmentCreateOrSelectOrganizationBinding.inflate(inflater)

    override fun getResultArguments(): Bundle =
        bundleOf(Arguments.ORGANIZATION_INPUT_DATA to viewModel.dataModel)

    override fun getViewPagerFragments(): ArrayList<ViewPagerFragment<*, CreateOrSelectOrganizationViewModel>> =
        arrayListOf(
            CreateOrganizationFragment(viewModel),
            SelectOrganizationFragment(viewModel)
        )
    //endregion

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.apply {
            newNameFlow.onEach {
                if(isCreateModelValid()) binding?.confirmButton?.enable()
            }.launchWhenStarted(lifecycleScope)

            productionFlow.onEach {
                if(isCreateModelValid()) binding?.confirmButton?.enable()
            }.launchWhenStarted(lifecycleScope)

            productItemFlow.onEach {
                if(isCreateModelValid()) binding?.confirmButton?.enable()
            }.launchWhenStarted(lifecycleScope)

            uuidFlow.onEach {
                if(isSelectModelValid())
                    binding?.confirmButton?.enable()
            }.launchWhenStarted(lifecycleScope)

            selectedNameFlow.onEach {
                if(isSelectModelValid()) binding?.confirmButton?.enable()
            }.launchWhenStarted(lifecycleScope)

            selectedViewPagerPageFlow.onEach {
                updateUi()
            }.launchWhenStarted(lifecycleScope)


           (requireArguments().getSerializable(Arguments.ORGANIZATION_INPUT_DATA) as OrganizationRequest?)?.apply {
                if (isSelectModelValid()) {
                    selectedViewPagerPage = ViewPagerPageNumber.SELECT_ORGANIZATION
                    setUuid(this.uuid)
                    setSelectedName(this.name)
                } else {
                    setNewName(this.name)
                    setProduction(this.production)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding?.apply {
            childFragmentManager.setFragmentResultListener(
                RequestKey.CREATE_OR_SELECT_ORGANIZATION_VIEW_PAGER_FRAGMENT,
                viewLifecycleOwner
            ){ _, arguments ->
                if (arguments.getBoolean(Arguments.IS_NEED_TO_HIDE_KEYBOARD))
                    hideKeyboard()

                if(arguments.getBoolean(Arguments.IS_NEED_TO_DISABLE_UI, false)){
                    createButton.disableWithoutTransparency()
                    selectButton.disableWithoutTransparency()
                }

                if(arguments.getBoolean(Arguments.IS_NEED_TO_ENABLE_UI, false)){
                    createButton.enable()
                    selectButton.enable()
                }

                if(arguments.getBoolean(Arguments.UPDATE_UI))
                    with(viewModel){
                        if(isCreateOrganizationPageSelected){
                            if(isCreateModelValid()) confirmButton.enable()
                            else confirmButton.disable()
                        } else if(isSelectOrganizationPageSelected){
                            if(isSelectModelValid()) confirmButton.enable()
                            else confirmButton.disable()
                        }
                    }
            }

            registerKeyboardBehaviorHandler(requireActivity())
            createBackButtonBackground(backButtonBackgroundImageView)
            rootConstraintLayout.setOnClickListener { hideKeyboard() }
            backgroundCardView.setOnClickListener { hideKeyboard() }
            contentConstraintLayout.setOnClickListener { hideKeyboard() }
            closeDialogButton.setOnClickListener { dismiss() }

            confirmButton.setOnClickListener {
                isDataShouldBeSaved = true
                dismiss()
            }

            organizationViewPager.apply {
                adapter = viewPagerAdapter
                isUserInputEnabled = false


                createButton.setOnClickListener {
                    viewModel.selectedViewPagerPage = ViewPagerPageNumber.CREATE_ORGANIZATION
                }
                selectButton.setOnClickListener {
                    viewModel.selectedViewPagerPage = ViewPagerPageNumber.SELECT_ORGANIZATION
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        instance = null
    }
    //endregion

    //region KeyboardBehaviorHandler
    override fun onShowKeyboard(keyboardHeight: Int) {
        binding?.confirmButton?.conceal()
    }

    override fun onHideKeyboard() {
        binding?.confirmButton?.show()
        dialog?.window?.currentFocus?.clearFocus()
    }
    //endregion

    //region UiUpdater
    override fun updateUi() {
        binding?.apply {
            organizationViewPager.currentItem = viewModel.selectedViewPagerPage

            if(viewModel.isCreateOrganizationPageSelected){
                createButton.setCardBackgroundColor(getColor(R.color.dark_dim_blue))
                selectButton.setCardBackgroundColor(getColor(R.color.transparent))
            } else {
                selectButton.setCardBackgroundColor(getColor(R.color.dark_dim_blue))
                createButton.setCardBackgroundColor(getColor(R.color.transparent))
            }
        }
    }
    //endregion
}