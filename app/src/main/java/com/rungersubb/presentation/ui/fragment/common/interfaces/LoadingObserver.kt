package com.rungersubb.presentation.ui.fragment.common.interfaces

import com.rungersubb.presentation.utils.enum.LoadingResultAction

interface LoadingObserver {
    fun onLoadingFinished(action: LoadingResultAction)
}