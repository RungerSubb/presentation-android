package com.rungersubb.presentation.utils.extension

import java.util.*

fun Calendar.fromDayMonthYear(day: Int, month: Int, year: Int): Calendar {
    set(Calendar.DAY_OF_MONTH, day)
    set(Calendar.MONTH, month)
    set(Calendar.YEAR, year)

    set(Calendar.HOUR_OF_DAY, 0)
    set(Calendar.MINUTE, 0)
    return this
}