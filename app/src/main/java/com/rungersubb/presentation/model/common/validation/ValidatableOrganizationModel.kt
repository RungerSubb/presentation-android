package com.rungersubb.presentation.model.common.validation

import com.rungersubb.presentation.utils.tools.UuidTool

interface ValidatableOrganizationModel: ValidatableModel {
    val uuid: String?
    val name: String?
    val production: List<String>?

    fun isUuidValid(): Boolean =
        UuidTool.isStringValid(uuid ?: String())

    fun isNameValid(): Boolean =
        name?.isNotEmpty() == true && name?.isNotBlank() == true

    fun isProductionValid(): Boolean =
        production?.isNotEmpty() == true

    override fun isModelValid(): Boolean =
        isCreateModelValid() || isSelectModelValid()

    fun isCreateModelValid(): Boolean =
        !isUuidValid() && isNameValid() && isProductionValid()

    fun isSelectModelValid(): Boolean =
        isUuidValid() && isNameValid()
}