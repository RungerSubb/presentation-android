package com.rungersubb.presentation.utils.tools

import android.content.Context
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.rungersubb.presentation.R
import kotlin.math.abs

class SwipeToDeleteCallback(
    private val context: Context,
    private val onSwipedAction: (Int) -> Unit): ItemTouchHelper.Callback(){

    companion object{
        private const val DEFAULT_DRAG_FLAGS = 0
        private const val EMPTY_DIMENSION = 0F
    }

    private val backgroundDrawable: Drawable = ColorDrawable().apply { color = getColor(R.color.dim_red) }
    private val iconDrawable: Drawable? = getDrawable(R.drawable.icon_delete)
    private val clearPaint: Paint = Paint().apply { xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR) }
    private val whitePaint: Paint = Paint().apply { colorFilter = PorterDuffColorFilter(getColor(R.color.white), PorterDuff.Mode.SRC_IN) }


    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int = makeMovementFlags(DEFAULT_DRAG_FLAGS, ItemTouchHelper.LEFT)

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean = false

    @Suppress("DEPRECATION")
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int): Unit =
        onSwipedAction(viewHolder.adapterPosition)


    override fun onChildDraw(
        canvas: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ): Unit = with(viewHolder.itemView) {
        super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        val isCancelled = dX == EMPTY_DIMENSION && !isCurrentlyActive

        if (isCancelled)
            clearCanvas(canvas, right + dX, top.toFloat(), right.toFloat(), bottom.toFloat())
        else {
            backgroundDrawable.apply {
                setBounds(right + dX.toInt(), top , right, bottom)
                draw(canvas)
            }

            val bitmapSize = (height - getDimension(R.dimen.padding_30dp)).toInt()
            val bitmapAdditionalHorizontalSpace = getDimension(R.dimen.margin_20dp)

            val isBitmapShouldBeDrawn = abs(dX) > bitmapSize + bitmapAdditionalHorizontalSpace

            if(isBitmapShouldBeDrawn) iconDrawable?.apply{
                val bitmap = this.toBitmap(bitmapSize, bitmapSize, Bitmap.Config.ARGB_8888)
                val bitmapItemBorderMargin = getDimension(R.dimen.margin_15dp)
                val bitmapTopMargin = top + bitmapItemBorderMargin
                val bitmapLeftMargin = (recyclerView.width - bitmapSize - bitmapItemBorderMargin)

                canvas.drawBitmap(bitmap, bitmapLeftMargin, bitmapTopMargin, whitePaint)
            }

        }
    }

    //region technical functions
    private fun clearCanvas(canvas: Canvas, left: Float, top: Float, right: Float, bottom: Float): Unit =
        canvas.drawRect(left, top, right, bottom, clearPaint)

    private fun getColor(resourceId: Int): Int =
        ContextCompat.getColor(context, resourceId)

    private fun getDrawable(resourceId: Int): Drawable? =
        ContextCompat.getDrawable(context, resourceId)

    private fun getDimension(resourceId: Int): Float =
        context.resources.getDimension(resourceId)
    //endregion

}