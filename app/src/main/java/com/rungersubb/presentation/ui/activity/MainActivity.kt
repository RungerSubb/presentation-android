package com.rungersubb.presentation.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.ActivityMainBinding
import com.rungersubb.presentation.mvvm.activity.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()
    private val mainNavController: NavController by lazy { findNavController(R.id.mainNavigationHost) }
    private val loginNavController: NavController by lazy { findNavController(R.id.loginNavigationHost) }
    private var binding: ActivityMainBinding? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //create binding
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding!!.root)


    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    override fun onBackPressed() {
        if(loginNavController.currentDestination?.id == R.id.signInFragment || loginNavController.currentDestination?.id == R.id.splashFragment)
            super.onBackPressed()
        else loginNavController.popBackStack()
    }
}