package com.rungersubb.presentation.network.repository

import com.rungersubb.presentation.model.request.SignInRequest
import com.rungersubb.presentation.model.request.SignUpRequest
import com.rungersubb.presentation.model.response.AuthenticationResponse
import com.rungersubb.presentation.network.api.AuthenticationApi
import com.rungersubb.presentation.network.repository.common.BasicRepository

class AuthenticationRepository(api: AuthenticationApi): BasicRepository<AuthenticationApi>(api) {
    suspend fun requestSignIn(request: SignInRequest): AuthenticationResponse =
        api.postAuthSignIn(request.asRequestBody())

    suspend fun requestSignUp(request: SignUpRequest): AuthenticationResponse =
        api.postAuthSignUp(request.asRequestBody())
}