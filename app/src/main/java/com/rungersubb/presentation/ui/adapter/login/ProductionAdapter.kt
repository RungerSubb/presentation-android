package com.rungersubb.presentation.ui.adapter.login

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rungersubb.presentation.databinding.HolderProductionBinding
import com.rungersubb.presentation.ui.adapter.common.BasicAdapter
import com.rungersubb.presentation.ui.adapter.common.DefaultActionAdapter

class ProductionAdapter(
    items: ArrayList<String> = arrayListOf(),
    action: (Int, String) -> Unit
): DefaultActionAdapter<String, ProductionAdapter.ProductionViewHolder>(items, action) {

    class ProductionViewHolder(binding: HolderProductionBinding) :
        BasicAdapter.BasicViewHolder<String, HolderProductionBinding>(binding) {
        override fun display(data: String): Unit =
            with(binding) {
                productionDataButton.text = data
            }

        override fun setOnClickListener(listener: View.OnClickListener?): Unit =
            binding.productionDataButton.setOnClickListener(listener)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductionViewHolder =
        ProductionViewHolder(HolderProductionBinding.inflate(LayoutInflater.from(parent.context)))
}