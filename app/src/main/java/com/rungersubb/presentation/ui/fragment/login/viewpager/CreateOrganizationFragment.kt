package com.rungersubb.presentation.ui.fragment.login.viewpager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.core.os.bundleOf
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.lifecycleScope
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.AlertDialogProductNameInputBinding
import com.rungersubb.presentation.databinding.VpFragmentCreateOrganizationBinding
import com.rungersubb.presentation.model.ui.ProductionItemModel
import com.rungersubb.presentation.mvvm.fragment.login.dialog.CreateOrSelectOrganizationViewModel
import com.rungersubb.presentation.ui.adapter.login.ProductionAdapter
import com.rungersubb.presentation.ui.fragment.common.viewpager.ViewPagerFragment
import com.rungersubb.presentation.ui.fragment.common.interfaces.AdapterHelper
import com.rungersubb.presentation.ui.fragment.common.interfaces.AlertDialogDisplayer
import com.rungersubb.presentation.ui.fragment.common.interfaces.EditTextStyleHelper
import com.rungersubb.presentation.ui.fragment.common.interfaces.UiUpdater
import com.rungersubb.presentation.utils.constants.*
import com.rungersubb.presentation.utils.enum.AdapterItemAction
import com.rungersubb.presentation.utils.extension.*
import com.rungersubb.presentation.utils.tools.keyboard.KeyboardBehaviorHandler
import kotlinx.coroutines.flow.onEach

class CreateOrganizationFragment(viewModel: CreateOrSelectOrganizationViewModel):
    ViewPagerFragment<VpFragmentCreateOrganizationBinding, CreateOrSelectOrganizationViewModel>(viewModel),
    AdapterHelper<ProductionAdapter, String>,
    AlertDialogDisplayer,
    EditTextStyleHelper,
    KeyboardBehaviorHandler,
    UiUpdater {

    override val adapter by lazy {
        ProductionAdapter{ index, item -> onAdapterItemClicked(index, item) }
    }

    private var productNameInputBinding: AlertDialogProductNameInputBinding? = null

    //region ViewPagerFragment
    override fun createBinding(inflater: LayoutInflater): VpFragmentCreateOrganizationBinding =
        VpFragmentCreateOrganizationBinding.inflate(inflater)

    override fun setViewPagerCurrentPage() {
        viewModel.selectedViewPagerPage = ViewPagerPageNumber.CREATE_ORGANIZATION
    }
    //endregion

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(viewModel){
            productionFlow.onEach { data ->
                adapter.update(data)
                updateUi()
            }.launchWhenStarted(lifecycleScope)

            productItemFlow.onEach { data ->
                data?.apply {
                    when(data.action){
                        AdapterItemAction.ADD -> adapter.add(item)
                        AdapterItemAction.INSERT -> adapter.insert(index, item)
                        AdapterItemAction.UPDATE -> adapter.update(index, item)
                        AdapterItemAction.REMOVE -> adapter.removeAt(index)
                    }
                }
                updateUi()
            }.launchWhenStarted(lifecycleScope)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding?.apply {
            registerKeyboardBehaviorHandler(requireActivity())
            rootConstraintLayout.setOnClickListener{
                setFragmentResult(
                    RequestKey.CREATE_OR_SELECT_ORGANIZATION_VIEW_PAGER_FRAGMENT,
                    bundleOf(Arguments.IS_NEED_TO_HIDE_KEYBOARD to true)
                )
            }

            organizationNameEditText.apply {
                    text = viewModel.newNameFlow.value?.toEditable()

                setOnFocusChangeListener { _, isFocused ->
                    if(!isFocused) {
                        val name = organizationNameEditText.text.toString()
                        if(name.isNotEmpty() && name.isBlank()) enableEditTextErrorStyle(organizationNameEditText)
                        viewModel.setNewName(name)
                    }
                    else enableEditTextDefaultStyle(organizationNameEditText)
                }
            }

            addProductionButton.setOnClickListener {
                disableUi()
                productNameInputBinding = displayProductNameInputDialog(
                    requireActivity(),
                    String(),
                    { newProductName ->
                        viewModel.productionItem(
                            ProductionItemModel(
                                newProductName,
                                CommonConstants.EMPTY_INT,
                                AdapterItemAction.ADD
                            )
                        )
                    },
                    {
                        productNameInputBinding = null
                        enableUi()
                    }
                )
            }

            hideKeyboardAreaView.setOnClickListener{
                setFragmentResult(
                    RequestKey.CREATE_OR_SELECT_ORGANIZATION_VIEW_PAGER_FRAGMENT,
                    bundleOf(Arguments.IS_NEED_TO_HIDE_KEYBOARD to true)
                )
            }

            productionRecyclerView.apply {
                this.layoutManager = layoutManager(requireContext())
                this.adapter = this@CreateOrganizationFragment.adapter
                enableSwipeToDeleteCallback(
                    requireActivity(),
                    this,
                    getString(R.string.Product_removed),
                    rootView,
                    { index ->
                        viewModel.productionItem(
                            ProductionItemModel(
                                String(),
                                index,
                                AdapterItemAction.REMOVE
                            )
                        )
                    },
                    { index, item ->
                        viewModel.productionItem(
                            ProductionItemModel(
                                item,
                                index,
                                AdapterItemAction.INSERT
                            )
                        )
                    }
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()
        setFragmentResult(
            RequestKey.CREATE_OR_SELECT_ORGANIZATION_VIEW_PAGER_FRAGMENT,
            bundleOf(Arguments.UPDATE_UI to true)
        )
    }
    //endregion

    //region KeyboardBehaviorHandler
    override fun onShowKeyboard(keyboardHeight: Int) {
        if (!AlertDialogDisplayer.isProductNameInputDialogDisplaying) {
            binding?.apply {
                productionCardView.disable()
                hideKeyboardAreaView.show()
            }
        }

        productNameInputBinding?.apply {
            rootRelativeLayout.post{
                rootRelativeLayout.apply {
                    setPadding(
                        paddingLeft,
                        paddingTop,
                        paddingRight,
                        keyboardHeight
                    )
                }
                confirmButton.apply {
                    updateLayoutParams<RelativeLayout.LayoutParams> {
                        addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, id)
                    }
                    alpha = AnimationExtra.ALPHA_VISIBLE
                }
                backgroundCardView.show()

            }
        }
    }

    override fun onHideKeyboard() {
        binding?.apply {
            productionCardView.enable()
            hideKeyboardAreaView.hide()
        }
        AlertDialogDisplayer.dismissProductNameInputDialog()
    }
    //endregion

    //region AdapterHelper
    override fun onAdapterItemClicked(index: Int, item: String) {
        disableUi()
        productNameInputBinding = displayProductNameInputDialog(
            requireActivity(),
            item,
            { newProductName ->
                viewModel.productionItem(
                    ProductionItemModel(
                        newProductName,
                        index,
                        AdapterItemAction.UPDATE
                    )
                )
            },
            {
                productNameInputBinding = null
                enableUi()
            }
        )
    }
    //endregion

    //region UiUpdater
    override fun updateUi(){
        binding?.apply {
            if(viewModel.production.isNotEmpty()) productionInfoTextView.hide()
            else productionInfoTextView.show()
        }
    }

    override fun enableUi() {
        setFragmentResult(
            RequestKey.CREATE_OR_SELECT_ORGANIZATION_VIEW_PAGER_FRAGMENT,
            bundleOf(Arguments.IS_NEED_TO_ENABLE_UI to true)
        )
    }

    override fun disableUi() {
        setFragmentResult(
            RequestKey.CREATE_OR_SELECT_ORGANIZATION_VIEW_PAGER_FRAGMENT,
            bundleOf(Arguments.IS_NEED_TO_DISABLE_UI to true)
        )
    }
    //endregion
}