package com.rungersubb.presentation.ui.fragment.common.dialog

import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.ViewModel
import androidx.viewbinding.ViewBinding
import com.rungersubb.presentation.utils.constants.Arguments

abstract class BackgroundNotifierDialogFragment<BINDING: ViewBinding, VIEW_MODEL: ViewModel>:
    BasicDialogFragment<BINDING, VIEW_MODEL>(){

    override fun onStart() {
        super.onStart()
        setFragmentResult(requestKey, bundleOf(Arguments.IS_NEED_TO_SHOW_DIALOG_FRAGMENT_BACKGROUND to true))
    }

}