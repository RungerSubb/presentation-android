package com.rungersubb.presentation.model.request

import com.google.gson.annotations.SerializedName
import com.rungersubb.presentation.model.request.common.RequestBodyModel

data class SignUpRequest(
    @SerializedName("user")
    val user: UserRequest,

    @SerializedName("organization")
    val organization: OrganizationRequest,

    @SerializedName("address")
    val address: AddressRequest?
): RequestBodyModel