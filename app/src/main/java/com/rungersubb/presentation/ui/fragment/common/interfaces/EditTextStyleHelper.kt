package com.rungersubb.presentation.ui.fragment.common.interfaces

import android.widget.EditText
import androidx.core.content.ContextCompat
import com.rungersubb.presentation.R

interface EditTextStyleHelper {
    fun enableEditTextErrorStyle(vararg editText: EditText): Unit =
        editText.forEach { item ->
            with(item) {
                background = ContextCompat.getDrawable(context, R.drawable.style_edittext_error)
                setTextColor(ContextCompat.getColor(context, R.color.dim_red))
            }
        }

    fun enableEditTextDefaultStyle(vararg editText: EditText): Unit =
        editText.forEach { item ->
            with(item) {
                background = ContextCompat.getDrawable(context, R.drawable.style_edittext_default)
                setTextColor(ContextCompat.getColor(context, R.color.black))
            }
        }
}