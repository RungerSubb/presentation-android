package com.rungersubb.presentation.model.request.common

import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody

interface RequestBodyModel {
    fun asJson(): String =
        Gson().toJson(this)

    fun asRequestBody(): RequestBody =
        asJson().toRequestBody("application/json".toMediaTypeOrNull())

}