package com.rungersubb.presentation.mvvm.common

interface DataInputHelper<DATA_MODEL: Any> {
    val dataModel: DATA_MODEL
}