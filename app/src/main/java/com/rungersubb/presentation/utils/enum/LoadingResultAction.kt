package com.rungersubb.presentation.utils.enum

enum class LoadingResultAction {
    LOGIN,
    CONTENT,
    ERROR
}