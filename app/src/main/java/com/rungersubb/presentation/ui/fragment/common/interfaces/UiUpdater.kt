package com.rungersubb.presentation.ui.fragment.common.interfaces

interface UiUpdater {
    fun updateUi(){}
    fun disableUi(){}
    fun enableUi(){}
}