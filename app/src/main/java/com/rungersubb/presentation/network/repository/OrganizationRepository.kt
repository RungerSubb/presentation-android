package com.rungersubb.presentation.network.repository

import com.rungersubb.presentation.model.request.PageDataRequest
import com.rungersubb.presentation.network.api.OrganizationApi
import com.rungersubb.presentation.network.repository.common.BasicRepository

class OrganizationRepository(api: OrganizationApi): BasicRepository<OrganizationApi>(api) {

    suspend fun requestOrganizationListCompact(request: PageDataRequest) =
        api.postOrganizationListCompactGet(request.asRequestBody())

}