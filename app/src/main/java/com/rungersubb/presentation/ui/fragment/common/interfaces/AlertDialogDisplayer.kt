package com.rungersubb.presentation.ui.fragment.common.interfaces

import android.app.Activity
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.addTextChangedListener
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.AlertDialogDatePickerBinding
import com.rungersubb.presentation.databinding.AlertDialogLoaderBinding
import com.rungersubb.presentation.databinding.AlertDialogProductNameInputBinding
import com.rungersubb.presentation.databinding.AlertDialogYesOrNoBinding
import com.rungersubb.presentation.utils.constants.AnimationExtra
import com.rungersubb.presentation.utils.extension.*
import org.joda.time.DateTime
import java.util.*

interface AlertDialogDisplayer {
    companion object{
        private var datePickerDialogInstance: AlertDialog? = null
        private var loaderDialogInstance: AlertDialog? = null
        private var productNameInputDialogInstance: AlertDialog? = null
        private var yesOrNoDialogInstance: AlertDialog? = null

        val isDatePickerDialogDisplaying: Boolean
            get() = datePickerDialogInstance != null
        val isLoaderDialogDisplaying: Boolean
            get() = loaderDialogInstance != null
        val isProductNameInputDialogDisplaying: Boolean
            get() = productNameInputDialogInstance != null
        val isYesOrNoDialogDisplaying: Boolean
            get() = yesOrNoDialogInstance != null

        fun dismissDatePickerDialog(){
            datePickerDialogInstance?.dismiss()
        }

        fun dismissDialogLoader(){
            loaderDialogInstance?.dismiss()
        }

        fun dismissProductNameInputDialog(){
            productNameInputDialogInstance?.dismiss()
        }

        fun dismissYesOrNoDialog(){
            yesOrNoDialogInstance?.dismiss()
        }

    }

    fun displayDatePickerDialog(
        activity: Activity,
        dateTime: DateTime? = null,
        onConfirmAction: (DateTime) -> Unit,
        onDismissAction: () -> Unit = {}
    ): AlertDialogDatePickerBinding? {
        if(isDatePickerDialogDisplaying) return null

        val binding =
            AlertDialogDatePickerBinding.inflate(LayoutInflater.from(activity)).apply {
                rootRelativeLayout.minimumHeight = activity.screenHeight
                backgroundCardView.setOnClickListener(null)
                datePicker.apply {
                    setOnClickListener(null)
                    (dateTime ?: DateTime()).apply{ init(year, month, dayOfMonth, null) }
                }
            }

        datePickerDialogInstance = AlertDialog.Builder(
            activity,
            R.style.DeveloperPresentation_DialogFullscreen
        ).apply {
            setView(binding.root)
            setCancelable(true)
        }.create().apply {
            window?.attributes?.windowAnimations = R.style.FadeFastDialogFragmentAnimations
            setOnDismissListener { datePickerDialogInstance = null }
            binding.apply {
                rootRelativeLayout.setOnClickListener { dismiss() }
                confirmButton.setOnClickListener {
                    onConfirmAction(
                        DateTime(
                            datePicker.run {
                                Calendar.getInstance().fromDayMonthYear(
                                    dayOfMonth,
                                    month,
                                    year
                                )
                            }
                        )
                    )
                    dismiss()
                }
            }
            show()
        }

        return binding
    }

    fun displayLoaderDialog(
        activity: Activity,
        onDismissAction: () -> Unit = {}
    ): AlertDialogLoaderBinding? {
        if(isLoaderDialogDisplaying) return null

        val binding = AlertDialogLoaderBinding.inflate(LayoutInflater.from(activity)).apply {
            rootLinearLayout.minimumHeight = activity.screenHeight
        }

        loaderDialogInstance = AlertDialog.Builder(
            activity,
            R.style.DeveloperPresentation_DialogFullscreen
        ).apply {
            setView(binding.root)
            setCancelable(false)
        }.create().apply {
            window?.attributes?.windowAnimations = R.style.FadeFastDialogFragmentAnimations
            setOnDismissListener { loaderDialogInstance = null }
            show()
        }

        return binding
    }

    fun displayProductNameInputDialog(
        activity: Activity,
        name: String = String(),
        onConfirmAction: (String) -> Unit,
        onDismissAction: () -> Unit = {},
    ): AlertDialogProductNameInputBinding? {
        if(isProductNameInputDialogDisplaying) return null

        val binding = AlertDialogProductNameInputBinding.inflate(LayoutInflater.from(activity)).apply {
            rootRelativeLayout.minimumHeight = activity.screenHeight
            backgroundCardView.apply {
                setOnClickListener(null)
                hide()
            }
            confirmButton.alpha = AnimationExtra.ALPHA_INVISIBLE
            productNameEditText.text = name.toEditable()
        }

        productNameInputDialogInstance = AlertDialog.Builder(
            activity,
            R.style.DeveloperPresentation_DialogFullscreen
        ).apply {
            setView(binding.root)
            setCancelable(true)
        }.create().apply {
            window?.attributes?.windowAnimations = R.style.FadeFastDialogFragmentAnimations
            setOnDismissListener {
                activity.hideKeyboard()
                onDismissAction()
                productNameInputDialogInstance = null
            }
            binding.apply {
                rootRelativeLayout.setOnClickListener { dismiss() }
                backgroundCardView.setOnClickListener{ activity.hideKeyboard() }
                productNameEditText.apply {
                    addTextChangedListener { text ->
                        val input = text?.toString() ?: String()

                        if(input.isNotEmpty() && input.isNotBlank()) confirmButton.apply {
                            enable()
                            show()
                        }
                        else confirmButton.apply {
                            disable()
                            conceal()
                        }
                    }
                    setSelection(name.length)
                }
                confirmButton.apply {
                    if(name.isEmpty() || name.isBlank()){
                        disable()
                        conceal()
                    }
                    setOnClickListener{
                        onConfirmAction(productNameEditText.text.toString())
                        dismiss()
                    }

                    root.post { activity.showKeyboard(productNameEditText) }
                }
                show()
            }
        }

        return binding
    }

    fun displayYesOrNoDialog(
        activity: Activity,
        label: String = String(),
        message: String = String(),
        onConfirmAction: () -> Unit,
        onDismissAction: () -> Unit = {}
    ): AlertDialogYesOrNoBinding?{
        if(isYesOrNoDialogDisplaying) return null

        val binding = AlertDialogYesOrNoBinding.inflate(LayoutInflater.from(activity)).apply {
            rootRelativeLayout.minimumHeight = activity.screenHeight
            labelTextView.text = label
            messageTextView.text = message
        }

        yesOrNoDialogInstance = AlertDialog.Builder(
            activity,
            R.style.DeveloperPresentation_DialogFullscreen
        ).apply {
            setView(binding.root)
            setCancelable(true)
        }.create().apply {
            window?.attributes?.windowAnimations = R.style.FadeFastDialogFragmentAnimations
            setOnDismissListener {
                onDismissAction()
                yesOrNoDialogInstance = null
            }

            binding.apply {
                rootRelativeLayout.setOnClickListener{ dismiss() }
                discardButton. setOnClickListener { dismiss() }
                contentCardView.setOnClickListener(null)
                confirmButton.setOnClickListener {
                    dismiss()
                    onConfirmAction()
                }
            }

            show()
        }

        return binding
    }
}