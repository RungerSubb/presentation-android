package com.rungersubb.presentation.model.request

import com.google.gson.annotations.SerializedName
import com.rungersubb.presentation.model.common.validation.ValidatableSignInModel
import com.rungersubb.presentation.model.request.common.RequestBodyModel

data class SignInRequest(
    @SerializedName("email")
    override val email: String,

    @SerializedName("password")
    override val password: String
): RequestBodyModel, ValidatableSignInModel