package com.rungersubb.presentation.ui.adapter.login

import android.view.LayoutInflater
import android.view.ViewGroup
import com.rungersubb.presentation.databinding.HolderSelectOrganizationBinding
import com.rungersubb.presentation.model.response.CompactOrganizationResponse
import com.rungersubb.presentation.ui.adapter.common.SelectableAdapter
import com.rungersubb.presentation.utils.extension.hide
import com.rungersubb.presentation.utils.extension.show

class SelectOrganizationAdapter(
    items: ArrayList<CompactOrganizationResponse?>,
    action: (Int, CompactOrganizationResponse?) -> Unit
): SelectableAdapter<CompactOrganizationResponse?, SelectOrganizationAdapter.SelectOrganizationViewHolder>(items, action) {

    class SelectOrganizationViewHolder(binding: HolderSelectOrganizationBinding):
        SelectableAdapter.SelectableViewHolder<CompactOrganizationResponse?, HolderSelectOrganizationBinding>(binding) {

        override fun display(data: CompactOrganizationResponse?): Unit =
            with(binding){
                data?.apply {
                    organizationNameTextView.text = name

                    organizationNameTextView.show()
                    organizationImageView.show()
                    isSelectedImageView.show()
                    horizontalLineView.show()

                    loadingProgressBar.hide()

                } ?: run {
                    organizationNameTextView.hide()
                    organizationImageView.hide()
                    isSelectedImageView.hide()
                    horizontalLineView.hide()

                    loadingProgressBar.show()
                }

            }


        override fun select(): Unit = binding.isSelectedImageView.show()
        override fun unselect(): Unit = binding.isSelectedImageView.hide()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectOrganizationViewHolder =
        SelectOrganizationViewHolder(HolderSelectOrganizationBinding.inflate(LayoutInflater.from(parent.context)))

    override fun isItemsAreEquals(
        firstItem: CompactOrganizationResponse?,
        secondItem: CompactOrganizationResponse?
    ): Boolean = firstItem?.uuid == secondItem?.uuid


    fun getItemByUuid(uuid: String): CompactOrganizationResponse? =
        items.firstOrNull { item -> item?.uuid == uuid }
}