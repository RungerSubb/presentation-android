package com.rungersubb.presentation.model.request

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.rungersubb.presentation.model.request.common.RequestBodyModel
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody

data class PageDataRequest(
    @SerializedName("page_number")
    val pageNumber: Int,

    @SerializedName("item_count_per_page")
    val itemCountPerPage: Int,

    @SerializedName("search_text")
    val searchText: String
): RequestBodyModel