package com.rungersubb.presentation.network.repository.common

import com.rungersubb.presentation.network.api.common.RetrofitApi

abstract class BasicRepository<API: RetrofitApi>(protected val api: API)