package com.rungersubb.presentation.model.request

import com.google.gson.annotations.SerializedName

data class UserRequest(
    @SerializedName("first_name")
    val firstName: String,

    @SerializedName("last_name")
    val lastName: String,

    @SerializedName("email")
    val email: String,

    @SerializedName("password")
    val password: String,

    @SerializedName("birth_date")
    val birthDate: Long,

    @SerializedName("user_type")
    val userType: String
)
