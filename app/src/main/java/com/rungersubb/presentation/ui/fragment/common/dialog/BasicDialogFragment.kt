package com.rungersubb.presentation.ui.fragment.common.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.ViewModel
import androidx.viewbinding.ViewBinding
import com.rungersubb.presentation.R
import com.rungersubb.presentation.ui.fragment.common.interfaces.ViewManager
import com.rungersubb.presentation.utils.extension.hideKeyboard

abstract class BasicDialogFragment<BINDING: ViewBinding, VIEW_MODEL: ViewModel>:
    DialogFragment(),
    ViewManager {

    protected var isDataShouldBeSaved: Boolean = false

    protected var binding: BINDING? = null
    protected abstract val viewModel: VIEW_MODEL
    protected abstract val requestKey: String
    protected abstract val animationStyle: Int

    protected abstract fun createBinding(inflater: LayoutInflater): BINDING
    protected open fun getResultArguments(): Bundle = bundleOf()

    override fun getTheme(): Int =
        R.style.DeveloperPresentation_DialogFullscreen

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = createBinding(inflater)
        return binding?.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            this.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        }
    }

    override fun onStart() {
        dialog?.window?.setWindowAnimations(animationStyle)
        super.onStart()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    final override fun onDismiss(dialog: DialogInterface) {
        setFragmentResult(requestKey, if(isDataShouldBeSaved) getResultArguments() else bundleOf())
        super.onDismiss(dialog)
    }

    protected fun hideKeyboard(){
        dialog?.window?.hideKeyboard()
    }

    protected fun getDrawable(resourceId: Int): Drawable? =
        ContextCompat.getDrawable(requireContext(), resourceId)

    protected fun getColor(resourceId: Int): Int =
        ContextCompat.getColor(requireContext(), resourceId)

    protected fun getDimension(resourceId: Int): Float =
        requireContext().resources.getDimension(resourceId)
}