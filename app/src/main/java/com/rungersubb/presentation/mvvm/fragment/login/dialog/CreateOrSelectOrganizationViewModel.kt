package com.rungersubb.presentation.mvvm.fragment.login.dialog

import androidx.lifecycle.viewModelScope
import com.rungersubb.presentation.model.common.validation.ValidatableOrganizationModel
import com.rungersubb.presentation.model.request.OrganizationRequest
import com.rungersubb.presentation.model.request.PageDataRequest
import com.rungersubb.presentation.model.response.CompactOrganizationResponse
import com.rungersubb.presentation.model.ui.ProductionItemModel
import com.rungersubb.presentation.mvvm.common.DataInputHelper
import com.rungersubb.presentation.mvvm.common.PaginationViewModel
import com.rungersubb.presentation.mvvm.common.ViewPagerViewModelHelper
import com.rungersubb.presentation.mvvm.common.network.OrganizationRepositoryProvider
import com.rungersubb.presentation.utils.constants.ViewPagerPageNumber
import com.rungersubb.presentation.utils.enum.AdapterItemAction
import com.rungersubb.presentation.utils.extension.launchSafe
import com.rungersubb.presentation.utils.extension.safeFromIndexOutOfBound
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class CreateOrSelectOrganizationViewModel:
    PaginationViewModel<CompactOrganizationResponse>(),
    DataInputHelper<OrganizationRequest>,
    OrganizationRepositoryProvider,
    ViewPagerViewModelHelper,
    ValidatableOrganizationModel {

    override var selectedViewPagerPage: Int
        get() = selectedViewPagerPageFlow.value
        set(value) { selectedViewPagerPageMutableFlow.value = value }

    val isCreateOrganizationPageSelected: Boolean
        get() = selectedViewPagerPage == ViewPagerPageNumber.CREATE_ORGANIZATION
    val isSelectOrganizationPageSelected: Boolean
        get() = selectedViewPagerPage == ViewPagerPageNumber.SELECT_ORGANIZATION


    override val uuid: String?
        get() = if(isSelectOrganizationPageSelected) uuidFlow.value else null
    override val name: String?
        get() = if(isCreateOrganizationPageSelected) newNameFlow.value else selectedNameFlow.value
    override val production: ArrayList<String> = arrayListOf()

    override val dataModel: OrganizationRequest
        get() = OrganizationRequest(uuid, name, production)

    //region Flow
    private val newNameMutableFlow: MutableStateFlow<String?> =
        MutableStateFlow(null)
    val newNameFlow: StateFlow<String?>
        get() = newNameMutableFlow.asStateFlow()

    private val productionMutableFlow: MutableStateFlow<ArrayList<String>> =
        MutableStateFlow(production)
    val productionFlow: StateFlow<ArrayList<String>>
        get() = productionMutableFlow.asStateFlow()

    private val productItemMutableFlow: MutableStateFlow<ProductionItemModel?> =
        MutableStateFlow(null)
    val productItemFlow: StateFlow<ProductionItemModel?>
        get() = productItemMutableFlow.asStateFlow()


    private val uuidMutableFlow: MutableStateFlow<String?> =
        MutableStateFlow(null)
    val uuidFlow: StateFlow<String?>
        get() = uuidMutableFlow.asStateFlow()

    private val selectedNameMutableFlow: MutableStateFlow<String?> =
        MutableStateFlow(null)
    val selectedNameFlow: StateFlow<String?>
        get() = selectedNameMutableFlow.asStateFlow()


    private val selectedViewPagerPageMutableFlow: MutableStateFlow<Int> =
        MutableStateFlow(ViewPagerPageNumber.CREATE_ORGANIZATION)
    val selectedViewPagerPageFlow: StateFlow<Int>
        get() = selectedViewPagerPageMutableFlow.asStateFlow()
    //endregion

    override fun executePageRequest(pageRequest: PageDataRequest) {
        viewModelScope.launchSafe (
            {
                val response = organizationRepository.requestOrganizationListCompact(pageRequest)
                handleResponse(response)
            },
            { handleResponse(listOf()) }
        )
    }

    //region setters
    fun setNewName(newName: String?){
        newNameMutableFlow.value = newName
    }

    fun setProduction(newProduction: ArrayList<String>){
        production.apply {
            clear()
            addAll(newProduction)
        }
        productionMutableFlow.value = production
    }

    fun productionItem(itemModel: ProductionItemModel): Unit =
        with(itemModel){
            when(action){
                AdapterItemAction.ADD -> production.add(item)
                AdapterItemAction.INSERT -> safeFromIndexOutOfBound{ production.add(index, item) }
                AdapterItemAction.UPDATE -> safeFromIndexOutOfBound { production[index] = item }
                AdapterItemAction.REMOVE -> safeFromIndexOutOfBound { production.removeAt(index) }
            }

            productItemMutableFlow.value = itemModel
        }

    fun setUuid(newUuid: String?){
        uuidMutableFlow.value = newUuid
    }

    fun setSelectedName(newName: String?){
        selectedNameMutableFlow.value = newName
    }
    //endregion
}