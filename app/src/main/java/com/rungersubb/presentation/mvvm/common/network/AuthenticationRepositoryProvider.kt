package com.rungersubb.presentation.mvvm.common.network

import com.rungersubb.presentation.application.PresentationApplication
import com.rungersubb.presentation.network.repository.AuthenticationRepository
import org.koin.android.ext.android.getKoin

interface AuthenticationRepositoryProvider {
    val authenticationRepository: AuthenticationRepository
        get() = PresentationApplication.get().getKoin().get()

    fun authenticate()
}