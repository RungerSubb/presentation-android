package com.rungersubb.presentation.mvvm.fragment.login.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rungersubb.presentation.R
import com.rungersubb.presentation.application.SharedPreferencesProvider
import com.rungersubb.presentation.model.enums.SignUpStepCondition
import com.rungersubb.presentation.model.enums.UserType
import com.rungersubb.presentation.model.request.AddressRequest
import com.rungersubb.presentation.model.request.OrganizationRequest
import com.rungersubb.presentation.model.request.SignUpRequest
import com.rungersubb.presentation.model.request.UserRequest
import com.rungersubb.presentation.model.response.AuthenticationResponse
import com.rungersubb.presentation.model.ui.SignUpStepModel
import com.rungersubb.presentation.model.ui.UserDataInputModel
import com.rungersubb.presentation.mvvm.common.LoginHelper
import com.rungersubb.presentation.mvvm.common.network.AuthenticationRepositoryProvider
import com.rungersubb.presentation.network.repository.AuthenticationRepository
import com.rungersubb.presentation.utils.extension.launchSafe
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class SignUpViewModel(private val sharedPreferencesProvider: SharedPreferencesProvider):
    ViewModel(),
    LoginHelper,
    AuthenticationRepositoryProvider {

    //region Flow
    private val userMutableFlow: MutableStateFlow<UserDataInputModel?> =
        MutableStateFlow(null)
    val userFlow: StateFlow<UserDataInputModel?>
        get() = userMutableFlow.asStateFlow()

    private val organizationMutableFlow: MutableStateFlow<OrganizationRequest?> =
        MutableStateFlow(null)
    val organizationFlow: StateFlow<OrganizationRequest?>
        get() = organizationMutableFlow.asStateFlow()

    private val addressMutableFlow: MutableStateFlow<AddressRequest?> =
        MutableStateFlow(null)
    val addressFlow: StateFlow<AddressRequest?>
        get() = addressMutableFlow.asStateFlow()


    private val authenticationMutableFlow: MutableStateFlow<AuthenticationResponse?> =
        MutableStateFlow(null)
    val authenticationFlow: StateFlow<AuthenticationResponse?>
        get() = authenticationMutableFlow.asStateFlow()
    //endregion

    //region Flow setters
    fun setUserValue(user: UserDataInputModel?){
        userMutableFlow.value = user
        sharedPreferencesProvider.saveUserRequest(user?.request())
    }

    fun setOrganizationValue(organization: OrganizationRequest?){
        organizationMutableFlow.value = organization
        sharedPreferencesProvider.saveOrganizationRequest(organization)
    }

    fun setAddressValue(address: AddressRequest?){
        addressMutableFlow.value = address
        sharedPreferencesProvider.saveAddressRequest(address)
    }
    //endregion

    //region get sign up steps
    private fun getUserSignUpStepStringResource(): Int =
        if(userFlow.value?.isModelValid() == true) R.string.sign_up_step_1_entered
        else R.string.sign_up_step_1

    private fun getOrganizationSignUpStepStringResource(): Int =
        if(organizationFlow.value?.isModelValid() == true){
            if(organizationFlow.value?.isCreateModelValid() == true) R.string.sign_up_step_2_entered
            else R.string.sign_up_step_2_selected
        }
        else R.string.sign_up_step_2

    private fun getAddressSignUpStepStringResource(): Int =
        if(organizationFlow.value?.isSelectModelValid() == true) R.string.sign_up_step_3
        else if(addressFlow.value?.isModelValid() == true) R.string.sign_up_step_3_entered
             else R.string.sign_up_step_3

    fun getUserSignUpStep(): SignUpStepModel =
        SignUpStepModel(
            getUserSignUpStepStringResource(),
            if(userFlow.value?.isModelValid() == true) SignUpStepCondition.ACCEPTED
            else SignUpStepCondition.DENIED
        )

    fun getOrganizationSignUpStep(): SignUpStepModel =
        SignUpStepModel(
            getOrganizationSignUpStepStringResource(),
            if(organizationFlow.value?.isModelValid() == true) SignUpStepCondition.ACCEPTED
            else SignUpStepCondition.DENIED
        )

    fun getAddressSignUpStep(): SignUpStepModel =
        SignUpStepModel(
            getAddressSignUpStepStringResource(),
            if(organizationFlow.value?.isSelectModelValid() == true) SignUpStepCondition.DISABLED
            else if(addressFlow.value?.isModelValid() == true) SignUpStepCondition.ACCEPTED
                 else SignUpStepCondition.DENIED
        )

    fun getSignUpSteps(): ArrayList<SignUpStepModel> =
        arrayListOf(
            getUserSignUpStep(),
            getOrganizationSignUpStep(),
            getAddressSignUpStep()
        )
    //endregion

    //region LoginHelper
    override fun loadSavedInputs(): Unit =
        with(sharedPreferencesProvider){
            userMutableFlow.value = UserDataInputModel.fromRequest(getUserRequest())
            organizationMutableFlow.value = getOrganizationRequest()
            addressMutableFlow.value = getAddressRequest()
        }

    override fun clearData(){
        userMutableFlow.value = null
        organizationMutableFlow.value = null
        addressMutableFlow.value = null

        with(sharedPreferencesProvider){
            saveUserRequest(null)
            saveOrganizationRequest(null)
            saveAddressRequest(null)
        }
    }

    override fun isInputCompleted(): Boolean =
        userFlow.value?.isModelValid() == true && organizationFlow.value?.isModelValid() == true

    override fun isNoDataEntered(): Boolean =
        !(userFlow.value?.isModelValid() ?: false) &&
        !(organizationFlow.value?.isModelValid() ?: false) &&
        !(addressFlow.value?.isModelValid() ?: false)
    //endregion

    //region AuthenticationRepositoryProvider
    override fun authenticate() {
        viewModelScope.launchSafe(
            {
                val request = SignUpRequest(
                    userFlow.value!!.request(
                        if(organizationFlow.value!!.isCreateModelValid()) UserType.OWNER.value
                        else UserType.USER.value
                    ),
                    organizationFlow.value!!,
                    addressFlow.value
                )

                val response = authenticationRepository.requestSignUp(request)
                authenticationMutableFlow.value = response
            },
            { authenticationMutableFlow.value = AuthenticationResponse() }
        )
    }
    //endregion
}