package com.rungersubb.presentation.application

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.google.gson.Gson
import com.rungersubb.presentation.model.request.AddressRequest
import com.rungersubb.presentation.model.request.OrganizationRequest
import com.rungersubb.presentation.model.request.UserRequest
import com.rungersubb.presentation.model.ui.UserDataInputModel
import com.rungersubb.presentation.utils.extension.safeFromJsonSyntax

class SharedPreferencesProvider(context: Context) {

    companion object{
        private const val SHARED_PREFERENCES_FILE_NAME = "SHARED_PREFERENCES"

        private const val ADDRESS_REQUEST = "PREFERENCE_ADDRESS_REQUEST"
        private const val EMAIL = "PREFERENCE_EMAIL"
        private const val ORGANIZATION_REQUEST = "PREFERENCE_ORGANIZATION_REQUEST"
        private const val PASSWORD = "PREFERENCE_PASSWORD"
        private const val TOKEN = "PREFERENCE_TOKEN"
        private const val USER_REQUEST = "PREFERENCE_USER_REQUEST"
    }

    private val preferences: SharedPreferences =
        EncryptedSharedPreferences.create(
            context,
            SHARED_PREFERENCES_FILE_NAME,
            MasterKey.Builder(context)
                .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
                .build(),
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    private val editor: SharedPreferences.Editor
        get() = preferences.edit()
    private val gson: Gson = Gson()

    //region save
    fun saveEmail(email: String?): Unit =
        with(editor){
            email?.also { data ->
                putString(EMAIL, data)
            } ?: remove(EMAIL)
            apply()
        }

    fun saveAddressRequest(addressInput: AddressRequest?): Unit =
        with(editor){
            addressInput?.also { data ->
                putString(ADDRESS_REQUEST, gson.toJson(data))
            } ?: remove(ADDRESS_REQUEST)
            apply()
        }

    fun saveOrganizationRequest(organizationInput: OrganizationRequest?): Unit =
        with(editor){
            organizationInput?.also { data ->
                putString(ORGANIZATION_REQUEST, gson.toJson(data))
            } ?: remove(ORGANIZATION_REQUEST)
            apply()
        }

    fun savePassword(password: String?): Unit =
        with(editor){
            password?.also { data ->
                putString(PASSWORD, data)
            } ?: remove(PASSWORD)
            apply()
        }

    fun saveToken(token: String?): Unit =
        with(editor){
            token?.also { data ->
                putString(TOKEN, data)
            } ?: remove(TOKEN)
            apply()
        }

    fun saveUserRequest(userInput: UserRequest?): Unit =
        with(editor){
            userInput?.also { data ->
                putString(USER_REQUEST, gson.toJson(data))
            } ?: remove(USER_REQUEST)
            apply()
        }
    //endregion

    //region get
    fun getEmail(): String? =
        preferences.getString(EMAIL, null)

    fun getAddressRequest(): AddressRequest? =
        safeFromJsonSyntax {
            gson.fromJson(
                preferences.getString(ADDRESS_REQUEST, String()),
                AddressRequest::class.java
            )
        }

    fun getOrganizationRequest(): OrganizationRequest? =
        safeFromJsonSyntax {
            gson.fromJson(
                preferences.getString(ORGANIZATION_REQUEST, String()),
                OrganizationRequest::class.java
            )
        }

    fun getPassword(): String? =
        preferences.getString(PASSWORD, null)

    fun getToken(): String? =
        preferences.getString(TOKEN, null)

    fun getUserRequest(): UserRequest? =
        safeFromJsonSyntax {
            gson.fromJson(
                preferences.getString(USER_REQUEST, String()),
                UserRequest::class.java
            )
        }
    //endregion

}