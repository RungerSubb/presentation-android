package com.rungersubb.presentation.model.enums

enum class UserType(val value: String) {
    USER("USER"),
    ADMIN("ADMIN"),
    OWNER("OWNER");
}