package com.rungersubb.presentation.model.enums

enum class SignUpStepCondition {
    DENIED,
    ACCEPTED,
    DISABLED
}