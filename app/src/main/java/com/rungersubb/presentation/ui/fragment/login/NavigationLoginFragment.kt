package com.rungersubb.presentation.ui.fragment.login

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.core.view.updateLayoutParams
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.FragmentNavigationLoginBinding
import com.rungersubb.presentation.mvvm.fragment.login.LoginNavigationViewModel
import com.rungersubb.presentation.ui.fragment.common.main.NavigationExtendedFragment
import com.rungersubb.presentation.ui.fragment.common.interfaces.DialogFragmentBackgroundHelper
import com.rungersubb.presentation.ui.fragment.common.interfaces.ViewAnimationHelper
import com.rungersubb.presentation.utils.constants.AnimationExtra
import com.rungersubb.presentation.utils.constants.Arguments
import com.rungersubb.presentation.utils.constants.RequestKey
import com.rungersubb.presentation.utils.extension.hideKeyboard
import org.koin.androidx.viewmodel.ext.android.viewModel


class NavigationLoginFragment:
    NavigationExtendedFragment<FragmentNavigationLoginBinding, LoginNavigationViewModel>(),
    ViewAnimationHelper<FragmentNavigationLoginBinding>,
    DialogFragmentBackgroundHelper {

    override val childNavController: NavController
        get() = Navigation.findNavController(binding!!.loginNavigationHost)

    override val viewModel: LoginNavigationViewModel by viewModel()

    //region NavigationExtendedFragment
    override fun createBinding(inflater: LayoutInflater): FragmentNavigationLoginBinding =
        FragmentNavigationLoginBinding.inflate(inflater)
    //endregion

    //region Lifecycle
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding?.run {
            childFragmentManager.apply {
                setFragmentResultListener(
                    RequestKey.LOADING_NAVIGATION_FRAGMENT,
                    viewLifecycleOwner){ _, resultArguments ->
                        val isLogoAnimationEnabled = resultArguments.getBoolean(Arguments.ENABLE_VIEW_ANIMATION)
                        val animator = if(isLogoAnimationEnabled) getValueAnimator(this@run) else null

                        val isNeedToNavigateToContent = resultArguments.getBoolean(Arguments.NAVIGATE_TO_CONTENT)
                        if(isNeedToNavigateToContent){
                            if(animator == null){
                                //TODO: Navigate to content action
                            } else animator.addListener(object : AnimatorListenerAdapter() {
                                override fun onAnimationEnd(animation: Animator?) {
                                    //TODO: Navigate to content action
                                }
                            })
                        }
                        animator?.start()
                }

                setFragmentResultListener(
                    RequestKey.SIGN_UP_FRAGMENT,
                    viewLifecycleOwner){ _, resultArguments ->
                    if (resultArguments.getBoolean(Arguments.IS_NEED_TO_SHOW_DIALOG_FRAGMENT_BACKGROUND))
                        showDialogFragmentBackground(dialogFragmentBackgroundView)
                    else hideDialogFragmentBackground(dialogFragmentBackgroundView)
                }
            }
            rootRelativeLayout.setOnClickListener{ requireActivity().hideKeyboard() }
        }
    }

    override fun onResume() {
        super.onResume()
        if(childDestination != R.id.splashFragment)
            binding?.mainLogoImageView?.updateLayoutParams<RelativeLayout.LayoutParams> {
                setMargins(leftMargin, resources.getDimension(R.dimen.margin_30dp).toInt(), rightMargin, bottomMargin)
            }
    }
    //endregion

    //region ViewAnimationHelper
    override fun getValueAnimator(binding: FragmentNavigationLoginBinding): ValueAnimator  =
        with(binding){
            ValueAnimator.ofInt(
                (mainLogoImageView.layoutParams as RelativeLayout.LayoutParams).topMargin,
                resources.getDimension(R.dimen.margin_30dp).toInt()
            ).apply {
                duration = AnimationExtra.DURATION_FROM_SPLASH_TO_SIGN_IN
                addUpdateListener { animator ->
                    val params = mainLogoImageView.layoutParams as RelativeLayout.LayoutParams
                    params.topMargin = animator.animatedValue as Int
                    mainLogoImageView.layoutParams = params
                }
            }
        }
    //endregion
}