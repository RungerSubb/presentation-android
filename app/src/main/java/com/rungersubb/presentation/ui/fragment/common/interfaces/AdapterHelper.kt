package com.rungersubb.presentation.ui.fragment.common.interfaces

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.rungersubb.presentation.R
import com.rungersubb.presentation.ui.adapter.common.BasicAdapter
import com.rungersubb.presentation.utils.extension.rootView
import com.rungersubb.presentation.utils.tools.SwipeToDeleteCallback

interface AdapterHelper<ADAPTER: BasicAdapter<ITEM, *>, ITEM: Any?>{

    val adapter: ADAPTER

    fun layoutManager(context: Context): LinearLayoutManager =
        LinearLayoutManager(context, RecyclerView.VERTICAL, false)


    fun onLastElementDisplayedListener(manager: LinearLayoutManager, action: ()-> Unit): View.OnScrollChangeListener =
        View.OnScrollChangeListener { _, _, _, _, _ ->
            val itemsCount = manager.itemCount
            val lastVisibleItemPosition = manager.findLastVisibleItemPosition()
            if(itemsCount > 1 && itemsCount - 1 == lastVisibleItemPosition)
                action()
        }

    fun enableSwipeToDeleteCallback(
        activity: Activity,
        recyclerView: RecyclerView,
        message: String,
        snackBarBaseView: View? = null,
        onRemovedAction: (Int) -> Unit,
        onRestoredAction: (Int, ITEM) -> Unit,
        onSnackbarShown: () -> Unit = {},
        onSnackbarDismissed: () -> Unit = {}
    ): Unit =
        ItemTouchHelper(
            SwipeToDeleteCallback(
                activity
            ){ position ->
                val item = adapter.get(position)
                if (item != null) {
                    val validSnackBarBaseView = snackBarBaseView ?: activity.rootView
                    onRemovedAction(position)
                    Snackbar.make(
                        validSnackBarBaseView,
                        message,
                        Snackbar.LENGTH_SHORT
                    ).apply {
                        setAction(activity.getString(R.string.UNDO)) {
                            onRestoredAction(position, item)
                        }
                        setActionTextColor(ContextCompat.getColor(activity, R.color.dim_orange))
                    }.show()
                }
            }
        ).attachToRecyclerView(recyclerView)


    fun onAdapterItemClicked(index: Int, item: ITEM)
}