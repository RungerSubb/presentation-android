package com.rungersubb.presentation.mvvm.common

interface ViewPagerViewModelHelper {
    var selectedViewPagerPage: Int
}