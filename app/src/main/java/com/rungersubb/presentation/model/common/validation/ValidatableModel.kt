package com.rungersubb.presentation.model.common.validation

interface ValidatableModel {
    fun isModelValid(): Boolean
}