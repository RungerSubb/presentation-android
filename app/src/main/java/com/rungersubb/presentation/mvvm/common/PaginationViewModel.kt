package com.rungersubb.presentation.mvvm.common

import androidx.lifecycle.ViewModel
import com.rungersubb.presentation.model.request.PageDataRequest
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

abstract class PaginationViewModel<LIST_ITEM: Any>: ViewModel() {

    companion object{
        private const val START_PAGE = 0
        private const val ITEM_COUNT_PER_PAGE = 10
    }

    private val listMutableFlow: MutableStateFlow<Pair<Boolean, ArrayList<LIST_ITEM?>>> =
        MutableStateFlow(Pair(true, arrayListOf(null)))
    val listFlow: StateFlow<Pair<Boolean, ArrayList<LIST_ITEM?>>>
        get() = listMutableFlow.asStateFlow()


    private var currentPage: Int = START_PAGE
    private var isLoadingNow: Boolean = false
    private var continueLoading: Boolean = true

    private fun pageRequest(searchText: String): PageDataRequest =
        PageDataRequest(currentPage, ITEM_COUNT_PER_PAGE, searchText)

    fun requestListNextPage(searchText: String){
        if(!isLoadingNow) {
            isLoadingNow = true
            if (continueLoading)
                executePageRequest(pageRequest(searchText))
        }
    }

    fun clear(){
        currentPage = START_PAGE
        continueLoading = true
        isLoadingNow = false

        listMutableFlow.value = Pair(true, arrayListOf())
    }

    protected fun handleResponse(response: List<LIST_ITEM>?) {
        if(response != null){
            val isNeedToClearOldDataInAdapter = currentPage == START_PAGE
            currentPage++

            val fullDataList = arrayListOf<LIST_ITEM?>().apply { addAll(response) }

            if (response.size < ITEM_COUNT_PER_PAGE)
                continueLoading = false
            if (continueLoading)
                fullDataList.add(null)

            listMutableFlow.value = Pair(isNeedToClearOldDataInAdapter, fullDataList)
        }
        isLoadingNow = false
    }

    protected abstract fun executePageRequest(pageRequest: PageDataRequest)
}