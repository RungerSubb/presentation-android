package com.rungersubb.presentation.application

import android.app.Application
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.context.loadKoinModules
import org.koin.core.context.stopKoin
import org.koin.core.context.unloadKoinModules

class PresentationApplication : Application() {

    companion object{
        private lateinit var instance: PresentationApplication

        fun get(): PresentationApplication =
            instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        startKoin {
            androidContext(applicationContext)
            modules(applicationModule, networkModule)
        }
    }


    fun reloadNetwork(){
        unloadKoinModules(networkModule)
        loadKoinModules(networkModule)
    }

}