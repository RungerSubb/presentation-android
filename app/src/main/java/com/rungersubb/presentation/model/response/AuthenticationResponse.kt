package com.rungersubb.presentation.model.response

import com.google.gson.annotations.SerializedName

data class AuthenticationResponse(
    @SerializedName("email")
    val email: String = String(),

    @SerializedName("token")
    val token: String = String()
)