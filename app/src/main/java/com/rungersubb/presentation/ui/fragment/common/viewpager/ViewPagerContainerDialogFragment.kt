package com.rungersubb.presentation.ui.fragment.common.viewpager

import androidx.lifecycle.ViewModel
import androidx.viewbinding.ViewBinding
import com.rungersubb.presentation.ui.adapter.common.ViewPagerFragmentAdapter
import com.rungersubb.presentation.ui.fragment.common.dialog.BackgroundNotifierDialogFragment

abstract class ViewPagerContainerDialogFragment<BINDING: ViewBinding, VIEW_MODEL: ViewModel>:
    BackgroundNotifierDialogFragment<BINDING, VIEW_MODEL>(){

    protected val viewPagerAdapter: ViewPagerFragmentAdapter<VIEW_MODEL> by lazy {
        ViewPagerFragmentAdapter(childFragmentManager, lifecycle, getViewPagerFragments())
    }

    abstract fun getViewPagerFragments(): ArrayList<ViewPagerFragment<*, VIEW_MODEL>>
}