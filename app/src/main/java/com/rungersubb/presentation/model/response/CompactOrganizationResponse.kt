package com.rungersubb.presentation.model.response

import com.google.gson.annotations.SerializedName

data class CompactOrganizationResponse(
    @SerializedName("uuid")
    val uuid: String = String(),

    @SerializedName("name")
    val name: String = String(),

    @SerializedName("production")
    val production: List<String> = listOf(),

    @SerializedName("owner_uuid")
    val ownerUuid: String = String()
)