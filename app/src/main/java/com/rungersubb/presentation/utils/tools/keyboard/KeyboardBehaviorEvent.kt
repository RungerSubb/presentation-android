package com.rungersubb.presentation.utils.tools.keyboard

import android.app.Activity
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import com.rungersubb.presentation.utils.constants.CommonConstants

object KeyboardBehaviorEvent {

    private fun globalLayoutListener(
        activity: Activity,
        handler: KeyboardBehaviorHandler
    ): ViewTreeObserver.OnGlobalLayoutListener =
        object : ViewTreeObserver.OnGlobalLayoutListener {

            private var wasOpened = false

            override fun onGlobalLayout() {
                val keyboardHeight = getKeyboardHeight(activity)
                val isOpen = isKeyboardVisible(activity, keyboardHeight)
                if (isOpen == wasOpened) { return }

                wasOpened = isOpen

                if(isOpen) handler.onShowKeyboard(keyboardHeight)
                else handler.onHideKeyboard()
            }
        }

    private fun getActivityRoot(activity: Activity): View {
        return getContentRoot(activity).rootView
    }

    private fun getContentRoot(activity: Activity): ViewGroup {
        return activity.findViewById(android.R.id.content)
    }

    private fun getKeyboardHeight(activity: Activity): Int {
        val rect = Rect()
        val activityRoot = getActivityRoot(activity)

        activityRoot.getWindowVisibleDisplayFrame(rect)

        val location = IntArray(2)
        getContentRoot(activity).getLocationOnScreen(location)

        val screenHeight = activityRoot.rootView.height
        return screenHeight - rect.height() - location[1]
    }

    private fun isKeyboardVisible(activity: Activity, keyboardHeight: Int): Boolean {
        val screenHeight = getActivityRoot(activity).rootView.height
        return keyboardHeight > screenHeight * CommonConstants.KEYBOARD_MIN_HEIGHT_RATIO
    }

    fun registerEventListener(activity: Activity, handler: KeyboardBehaviorHandler){
        val activityRoot = getActivityRoot(activity)
        val layoutListener = globalLayoutListener(activity, handler)

        activityRoot.viewTreeObserver.addOnGlobalLayoutListener(layoutListener)
    }

}