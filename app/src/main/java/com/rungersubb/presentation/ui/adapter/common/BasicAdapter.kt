package com.rungersubb.presentation.ui.adapter.common

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.rungersubb.presentation.utils.extension.safeFromIndexOutOfBound

abstract class BasicAdapter<ITEM: Any?, HOLDER: BasicAdapter.BasicViewHolder<ITEM, *>>(
        protected val items: ArrayList<ITEM>
    ): RecyclerView.Adapter<HOLDER>() {

    abstract class BasicViewHolder<ITEM: Any?, BINDING: ViewBinding>(protected val binding: BINDING):
        RecyclerView.ViewHolder(binding.root){
            protected val context: Context
                get() = binding.root.context

            abstract fun display(data: ITEM)

            open fun setOnClickListener(listener: View.OnClickListener?): Unit =
                binding.root.setOnClickListener(listener)


            protected fun getString(resourceId: Int): String =
                context.getString(resourceId)

            protected fun getColor(resourceId: Int): Int =
                ContextCompat.getColor(context, resourceId)
    }

    final override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: HOLDER, position: Int): Unit =
        holder.display(items[position])

    //region update
    @SuppressLint("NotifyDataSetChanged")
    fun update(newItems: Collection<ITEM> = listOf()) {
        with(items) {
            clear()
            addAll(newItems)
        }

        notifyDataSetChanged()
    }

    fun update(index: Int, item: ITEM) {
        safeFromIndexOutOfBound {
            items[index] = item
            notifyItemChanged(index)
        }
    }
    //endregion

    //region add
    @SuppressLint("NotifyDataSetChanged")
    fun add(newItems: Collection<ITEM> = listOf()){
        items.apply {
            addAll(newItems)
            val distinct = distinct()
            clear()
            addAll(distinct)
        }
        notifyDataSetChanged()
    }

    fun add(item: ITEM){
        items.add(item)
        notifyItemChanged(items.lastIndex)
    }

    fun insert(position: Int, item: ITEM){
        safeFromIndexOutOfBound {
            items.add(position, item)
            notifyItemInserted(position)
            notifyItemRangeChanged(position, items.size)
        }
    }
    //endregion

    //region remove
    fun removeAt(position: Int){
        safeFromIndexOutOfBound {
            items.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, items.size)
        }
    }

    fun removeLastIfItNull(){
        if(items.isNotEmpty())
            if(items[items.lastIndex] == null)
                removeAt(items.lastIndex)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clear(){
        items.clear()
        notifyDataSetChanged()
    }
    //endregion

    fun get(position: Int): ITEM? {
        return safeFromIndexOutOfBound { items[position] }
    }


    protected fun getItemIndex(item: ITEM?): Int? {
        var index: Int? = null
        items.forEachIndexed { currentIndex, currentItem ->
            if(isItemsAreEquals(item, currentItem)){
                index = currentIndex
                return@forEachIndexed
            }
        }

        return index
    }

    protected open fun isItemsAreEquals(firstItem: ITEM?, secondItem: ITEM?): Boolean =
        firstItem == secondItem
}