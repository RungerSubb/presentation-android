package com.rungersubb.presentation.ui.fragment.common.viewpager

import androidx.lifecycle.ViewModel
import androidx.viewbinding.ViewBinding
import com.rungersubb.presentation.ui.fragment.common.main.ViewModelFragment

abstract class ViewPagerFragment<BINDING: ViewBinding, VIEW_MODEL: ViewModel>(override val viewModel: VIEW_MODEL):
    ViewModelFragment<BINDING, VIEW_MODEL>() {

    abstract fun setViewPagerCurrentPage()

    override fun onResume() {
        super.onResume()
        setViewPagerCurrentPage()
    }

}