package com.rungersubb.presentation.utils.extension

import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.EmptyCoroutineContext

fun CoroutineScope.launchSafe(
    action: suspend CoroutineScope.() -> Unit,
    onSaved: suspend CoroutineScope.() -> Unit
): Job {
    return launch(EmptyCoroutineContext, CoroutineStart.DEFAULT) {
        try { action() }
        catch (exception: Exception) {
            Log.i("SAVED FROM", "Exception")
            onSaved()
        }
    }
}