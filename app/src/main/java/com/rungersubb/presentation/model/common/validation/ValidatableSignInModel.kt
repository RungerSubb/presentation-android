package com.rungersubb.presentation.model.common.validation

import com.rungersubb.presentation.utils.constants.RegexValues
import com.rungersubb.presentation.utils.tools.RegexValidator

interface ValidatableSignInModel: ValidatableModel {
    val email: String
    val password: String

    fun isEmailValid(): Boolean =
        email.isNotEmpty() && email.isNotBlank() && RegexValidator.isStringValidByRegex(email, RegexValues.EMAIL)

    fun isPasswordValid(): Boolean =
        RegexValidator.isStringValidByRegex(password, RegexValues.PASSWORD)

    override fun isModelValid(): Boolean =
        isEmailValid() && isPasswordValid()
}