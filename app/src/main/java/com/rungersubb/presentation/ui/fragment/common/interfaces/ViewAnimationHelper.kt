package com.rungersubb.presentation.ui.fragment.common.interfaces

import android.animation.ValueAnimator
import androidx.viewbinding.ViewBinding

interface ViewAnimationHelper<BINDING: ViewBinding>{
    fun getValueAnimator(binding: BINDING): ValueAnimator
}