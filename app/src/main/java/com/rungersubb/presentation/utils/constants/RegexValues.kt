package com.rungersubb.presentation.utils.constants

object RegexValues {
    const val DIGITS = "^[0-9 ]+"
    const val EMAIL = "^(.+)@(.+)$"
    const val PASSWORD = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$"
}