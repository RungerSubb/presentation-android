package com.rungersubb.presentation.utils.enum

enum class AdapterItemAction {
    ADD,
    INSERT,
    UPDATE,
    REMOVE,
}