package com.rungersubb.presentation.utils.extension

import android.text.Editable
import android.text.SpannableStringBuilder

fun String.toEditable(): Editable =
    SpannableStringBuilder(this)