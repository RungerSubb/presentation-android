package com.rungersubb.presentation.utils.tools

import java.util.regex.Pattern

object RegexValidator {
        fun isStringValidByRegex(data: String, regex: String): Boolean =
            Pattern.compile(regex).matcher(data).matches()
}