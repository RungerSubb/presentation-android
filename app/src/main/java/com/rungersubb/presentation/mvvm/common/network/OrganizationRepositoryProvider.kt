package com.rungersubb.presentation.mvvm.common.network

import com.rungersubb.presentation.application.PresentationApplication
import com.rungersubb.presentation.network.repository.OrganizationRepository
import org.koin.android.ext.android.getKoin

interface OrganizationRepositoryProvider {
    val organizationRepository: OrganizationRepository
        get() = PresentationApplication.get().getKoin().get()
}