package com.rungersubb.presentation.utils.extension

import org.joda.time.DateTime

val DateTime.displayingDateString: String
    get() {
        val day = if(dayOfMonth > 9) dayOfMonth.toString() else "0$dayOfMonth"
        val month = if (monthOfYear > 9) monthOfYear.toString() else "0$monthOfYear"

        return "$day.$month.$year"
    }

val DateTime.displayingDateTimeString: String
    get(){
        val hour = if(hourOfDay > 9) hourOfDay.toString() else "0$hourOfDay"
        val minute = if(minuteOfHour > 9) minuteOfHour.toString() else "0$minuteOfHour"

        return "$displayingDateString\t$hour:$minute"
    }