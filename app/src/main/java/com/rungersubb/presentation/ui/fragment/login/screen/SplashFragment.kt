package com.rungersubb.presentation.ui.fragment.login.screen

import android.animation.ValueAnimator
import android.view.LayoutInflater
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.FragmentSplashBinding
import com.rungersubb.presentation.mvvm.fragment.login.screens.SplashViewModel
import com.rungersubb.presentation.ui.fragment.common.interfaces.LoadingObserver
import com.rungersubb.presentation.ui.fragment.common.main.NavigationFragment
import com.rungersubb.presentation.ui.fragment.common.interfaces.ViewAnimationHelper
import com.rungersubb.presentation.utils.constants.AnimationExtra
import com.rungersubb.presentation.utils.constants.Arguments
import com.rungersubb.presentation.utils.constants.RequestKey
import com.rungersubb.presentation.utils.enum.LoadingResultAction
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.concurrent.thread

class SplashFragment:
    NavigationFragment<FragmentSplashBinding, SplashViewModel>(),
    ViewAnimationHelper<FragmentSplashBinding>,
    LoadingObserver {

    override val viewModel: SplashViewModel by viewModel()

    //region NavigationFragment
    override fun createBinding(inflater: LayoutInflater): FragmentSplashBinding =
        FragmentSplashBinding.inflate(inflater)
    //endregion

    //region Lifecycle
    override fun onResume() {
        super.onResume()
        thread {
            Thread.sleep(2000)
            requireActivity().runOnUiThread { onLoadingFinished(LoadingResultAction.LOGIN) }
        }
    }
    //endregion

    //region ViewAnimationHelper
    override fun getValueAnimator(binding: FragmentSplashBinding): ValueAnimator =
        with(binding){
            return@with ValueAnimator.ofFloat(
                AnimationExtra.ALPHA_VISIBLE,
                AnimationExtra.ALPHA_INVISIBLE
            ).apply {
                duration = AnimationExtra.DURATION_FROM_SPLASH_TO_SIGN_IN
                addUpdateListener { animator ->
                    loadingProgressBar.alpha = animator.animatedValue as Float
                }
            }
        }
    //endregion

    //region LoadingObserver
    override fun onLoadingFinished(action: LoadingResultAction){
        when(action){
            LoadingResultAction.LOGIN -> {
                parentFragment?.setFragmentResult(
                    RequestKey.LOADING_NAVIGATION_FRAGMENT,
                    bundleOf(Arguments.ENABLE_VIEW_ANIMATION to true)
                )

                navigate(R.id.signInFragment, bundleOf(
                    Arguments.ENABLE_VIEW_ANIMATION to true
                ))
            }
            LoadingResultAction.CONTENT -> {
                parentFragment?.setFragmentResult(
                    RequestKey.LOADING_NAVIGATION_FRAGMENT,
                    bundleOf(
                        Arguments.ENABLE_VIEW_ANIMATION to true,
                        Arguments.NAVIGATE_TO_CONTENT to true
                    )
                )
                binding?.apply { getValueAnimator(this).start() }

            }
            LoadingResultAction.ERROR -> {
                //TODO: go to error
            }
        }
    }
    //endregion
}