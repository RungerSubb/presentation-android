package com.rungersubb.presentation.ui.adapter.common

import androidx.viewbinding.ViewBinding
import com.rungersubb.presentation.utils.constants.CommonConstants

abstract class SelectableAdapter<ITEM: Any?, HOLDER: SelectableAdapter.SelectableViewHolder<ITEM, *>>(
    items: ArrayList<ITEM>,
    action: (Int, ITEM) -> Unit
): DefaultActionAdapter<ITEM, HOLDER>(items, action) {

    abstract class SelectableViewHolder<ITEM: Any?, BINDING: ViewBinding>(binding: BINDING):
        BasicViewHolder<ITEM, BINDING>(binding){

            abstract fun select()
            abstract fun unselect()
    }

    private var selectedItem: ITEM? = null

    fun selectedItem(): ITEM? = selectedItem
    fun selectItem(item: ITEM?){
        selectedItem = item
        getItemIndex(item)?.apply { notifyItemChanged(this) }
    }

    override fun onBindViewHolder(holder: HOLDER, position: Int) {
        val item = items[position]
        val isCurrentItemSelected = isItemsAreEquals(item, selectedItem)
        with(holder){
            display(item)

            if(isCurrentItemSelected && item != null) select()
            else unselect()

            setOnClickListener{
                if(!isCurrentItemSelected){
                    if(selectedItem != null){
                        val itemIndex = getItemIndex(selectedItem)
                        if (itemIndex != null) notifyItemChanged(itemIndex)
                    }
                    action(position, item)
                }
            }
        }
    }
}