package com.rungersubb.presentation.utils.extension

fun <T> Collection<T>.toArrayList(): ArrayList<T> =
    ArrayList<T>().apply { addAll(this@toArrayList) }
