package com.rungersubb.presentation.ui.view.common

import android.content.res.TypedArray
import androidx.viewbinding.ViewBinding

interface CoreView<BINDING: ViewBinding> {

    val binding: BINDING
    fun setupWithAttributes(attributes: TypedArray)
}