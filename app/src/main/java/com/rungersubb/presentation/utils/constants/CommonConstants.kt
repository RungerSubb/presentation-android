package com.rungersubb.presentation.utils.constants

object CommonConstants {
    const val EMPTY_INT = -1
    const val CREATE_ORGANIZATION_UI_UPDATE_DELAY = 1000L
    const val DEFAULT_BIRTH_DATE = 0L
    const val KEYBOARD_MIN_HEIGHT_RATIO = 0.15

    const val TOKEN_BEARER = "RungerSubbPresentation"
}