package com.rungersubb.presentation.ui.adapter.common

import com.rungersubb.presentation.utils.extension.safeFromIndexOutOfBound

abstract class DefaultActionAdapter<ITEM: Any?, HOLDER: BasicAdapter.BasicViewHolder<ITEM, *>>(
    items: ArrayList<ITEM> = arrayListOf(),
    protected val action: (index: Int, item: ITEM) -> Unit
): BasicAdapter<ITEM, HOLDER>(items) {

    override fun onBindViewHolder(holder: HOLDER, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.setOnClickListener{
            safeFromIndexOutOfBound { action(position, items[position]) }
        }
    }

}