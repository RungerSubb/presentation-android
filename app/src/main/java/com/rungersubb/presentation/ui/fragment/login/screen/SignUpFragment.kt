package com.rungersubb.presentation.ui.fragment.login.screen

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.lifecycleScope
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.FragmentSignUpBinding
import com.rungersubb.presentation.model.request.AddressRequest
import com.rungersubb.presentation.model.request.OrganizationRequest
import com.rungersubb.presentation.model.ui.SignUpStepModel
import com.rungersubb.presentation.model.ui.UserDataInputModel
import com.rungersubb.presentation.mvvm.fragment.login.screens.SignUpViewModel
import com.rungersubb.presentation.ui.adapter.login.SignUpStepsAdapter
import com.rungersubb.presentation.ui.fragment.common.main.NavigationFragment
import com.rungersubb.presentation.ui.fragment.common.interfaces.AdapterHelper
import com.rungersubb.presentation.ui.fragment.common.interfaces.AlertDialogDisplayer
import com.rungersubb.presentation.ui.fragment.common.interfaces.DialogFragmentBackgroundHelper
import com.rungersubb.presentation.ui.fragment.login.dialog.CreateOrSelectOrganizationDialogFragment
import com.rungersubb.presentation.ui.fragment.login.dialog.EnterAddressDataDialogFragment
import com.rungersubb.presentation.ui.fragment.login.dialog.EnterUserDataDialogFragment
import com.rungersubb.presentation.utils.constants.Arguments
import com.rungersubb.presentation.utils.constants.CommonConstants
import com.rungersubb.presentation.utils.constants.RequestKey
import com.rungersubb.presentation.utils.extension.*
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignUpFragment:
    NavigationFragment<FragmentSignUpBinding, SignUpViewModel>(),
    AdapterHelper<SignUpStepsAdapter, SignUpStepModel>,
    DialogFragmentBackgroundHelper.Notifier,
    AlertDialogDisplayer {

    override val viewModel: SignUpViewModel by viewModel()
    override val adapter: SignUpStepsAdapter by lazy {
        SignUpStepsAdapter(viewModel.getSignUpSteps()){ index: Int, item: SignUpStepModel ->
            onAdapterItemClicked(index, item)
        }
    }

    //region NavigationFragment
    override fun createBinding(inflater: LayoutInflater): FragmentSignUpBinding =
        FragmentSignUpBinding.inflate(inflater)
    //endregion

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(viewModel){
            userFlow.onEach {
                adapter.update(SignUpStepsAdapter.ENTER_USER_DATA_INDEX, getUserSignUpStep())

                binding?.apply {
                    if(viewModel.isInputCompleted()) signUpButton.enable()
                    else signUpButton.disable()

                    if(isNoDataEntered()) clearInputTextView.hide()
                    else clearInputTextView.show()
                }
            }.launchWhenStarted(lifecycleScope)

            organizationFlow.onEach {
                adapter.apply {
                    update(SignUpStepsAdapter.ENTER_ORGANIZATION_DATA_INDEX, viewModel.getOrganizationSignUpStep())
                    update(SignUpStepsAdapter.ENTER_ADDRESS_DATA_INDEX, viewModel.getAddressSignUpStep())
                }

                binding?.apply {
                    if(viewModel.isInputCompleted()) signUpButton.enable()
                    else signUpButton.disable()

                    if(isNoDataEntered()) clearInputTextView.hide()
                    else clearInputTextView.show()
                }
            }.launchWhenStarted(lifecycleScope)

            addressFlow.onEach {
                adapter.update(SignUpStepsAdapter.ENTER_ADDRESS_DATA_INDEX, viewModel.getAddressSignUpStep())

                binding?.apply {
                    if(viewModel.isInputCompleted()) signUpButton.enable()
                    else signUpButton.disable()

                    if(isNoDataEntered()) clearInputTextView.hide()
                    else clearInputTextView.show()
                }
            }.launchWhenStarted(lifecycleScope)

            authenticationFlow.onEach { data ->
                if(!AlertDialogDisplayer.isLoaderDialogDisplaying) return@onEach

                if(data?.token?.startsWith(CommonConstants.TOKEN_BEARER) == true && AlertDialogDisplayer.isLoaderDialogDisplaying)
                    Toast.makeText(requireContext(), "AUTHENTICATED", Toast.LENGTH_SHORT).show()
                else if(AlertDialogDisplayer.isLoaderDialogDisplaying)
                    Toast.makeText(requireContext(), "ERROR", Toast.LENGTH_SHORT).show()

                if(AlertDialogDisplayer.isLoaderDialogDisplaying)
                    AlertDialogDisplayer.dismissDialogLoader()
            }.launchWhenStarted(lifecycleScope)

            viewModel.loadSavedInputs()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding?.apply {

            childFragmentManager.apply {
                setFragmentResultListener(
                    RequestKey.ENTER_USER_DATA_DIALOG_FRAGMENT,
                    viewLifecycleOwner){ _, arguments ->

                    notifyParentForDialog(
                        parentFragment,
                        RequestKey.SIGN_UP_FRAGMENT,
                        arguments.getBoolean(Arguments.IS_NEED_TO_SHOW_DIALOG_FRAGMENT_BACKGROUND)
                    )

                    (arguments.getSerializable(Arguments.USER_INPUT_DATA) as UserDataInputModel?)?.also { input ->
                        viewModel.setUserValue(input)
                    }

                    registerRecyclerView.enable()
                }

                setFragmentResultListener(
                    RequestKey.SELECT_OR_CREATE_ORGANIZATION_DIALOG_FRAGMENT,
                    viewLifecycleOwner){_, arguments ->

                    notifyParentForDialog(
                        parentFragment,
                        RequestKey.SIGN_UP_FRAGMENT,
                        arguments.getBoolean(Arguments.IS_NEED_TO_SHOW_DIALOG_FRAGMENT_BACKGROUND)
                    )

                    (arguments.getSerializable(Arguments.ORGANIZATION_INPUT_DATA) as OrganizationRequest?)?.also { input ->
                        viewModel.setOrganizationValue(input)
                    }

                    registerRecyclerView.enable()
                }

                setFragmentResultListener(
                    RequestKey.ENTER_ADDRESS_DATA_DIALOG_FRAGMENT,
                    viewLifecycleOwner){ _, arguments ->

                    notifyParentForDialog(
                        parentFragment,
                        RequestKey.SIGN_UP_FRAGMENT,
                        arguments.getBoolean(Arguments.IS_NEED_TO_SHOW_DIALOG_FRAGMENT_BACKGROUND)
                    )

                    (arguments.getSerializable(Arguments.ADDRESS_INPUT_DATA) as AddressRequest?)?.also { input ->
                        viewModel.setAddressValue(input)
                    }

                    registerRecyclerView.enable()
                }
            }

            registerRecyclerView.apply {
                this.layoutManager = layoutManager(requireContext())
                this.adapter = this@SignUpFragment.adapter
            }

            signUpButton.apply {
                setOnClickListener {
                    displayLoaderDialog(requireActivity())
                    viewModel.authenticate()
                }
                if(!viewModel.isInputCompleted()) disable()
            }

            clearInputTextView.apply {
                if (viewModel.isNoDataEntered()) hide()

                setOnTouchListener { _, motionEvent ->
                    when(motionEvent.action){
                        MotionEvent.ACTION_DOWN -> {
                            typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
                            text = SpannableStringBuilder(text.toString()).apply {
                                setSpan(UnderlineSpan(), 0, text.length, 0)
                            }
                        }
                        MotionEvent.ACTION_UP -> {
                            typeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL)
                            text = getString(R.string.Clear)
                        }
                    }

                    false
                }

                setOnClickListener {
                    displayYesOrNoDialog(
                        requireActivity(),
                        getString(R.string.Clear_input_label),
                        getString(R.string.Clear_input_message),
                        { viewModel.clearData() }
                    )
                }
            }

            backToLoginButton.setOnClickListener {
                requireActivity().onBackPressed()
            }
        }
    }
    //endregion

    //region AdapterHelper
    override fun onAdapterItemClicked(index: Int, item: SignUpStepModel) {
        when(index){
            SignUpStepsAdapter.ENTER_USER_DATA_INDEX -> EnterUserDataDialogFragment.display(
                childFragmentManager,
                bundleOf(Arguments.USER_INPUT_DATA to viewModel.userFlow.value)
            )
            SignUpStepsAdapter.ENTER_ORGANIZATION_DATA_INDEX -> CreateOrSelectOrganizationDialogFragment.display(
                childFragmentManager,
                bundleOf(Arguments.ORGANIZATION_INPUT_DATA to viewModel.organizationFlow.value)
            )
            SignUpStepsAdapter.ENTER_ADDRESS_DATA_INDEX -> EnterAddressDataDialogFragment.display(
                childFragmentManager,
                bundleOf(Arguments.ADDRESS_INPUT_DATA to viewModel.addressFlow.value)
            )
        }
    }
    //endregion
}