package com.rungersubb.presentation.model.request

import com.google.gson.annotations.SerializedName
import com.rungersubb.presentation.model.common.validation.ValidatableAddressModel
import java.io.Serializable

data class AddressRequest(
    @SerializedName("country")
    override val country: String,

    @SerializedName("region")
    override val region: String,

    @SerializedName("city")
    override val city: String,

    @SerializedName("street")
    override val street: String,

    @SerializedName("building")
    override val building: String,

    @SerializedName("office")
    override val office: String,

    @SerializedName("zip_code")
    override val zipCode: String
): Serializable, ValidatableAddressModel