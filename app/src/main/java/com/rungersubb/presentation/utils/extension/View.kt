package com.rungersubb.presentation.utils.extension

import android.view.View

fun View.show(){
    visibility = View.VISIBLE
}

fun View.hide(){
    visibility = View.GONE
}

fun View.conceal(){
    visibility = View.INVISIBLE
}

fun View.enable(){
    isEnabled = true
    alpha = 1F
}

fun View.disable() {
    isEnabled = false
    alpha = 0.5F
}

fun View.disableWithoutTransparency(){
    isEnabled = false
}