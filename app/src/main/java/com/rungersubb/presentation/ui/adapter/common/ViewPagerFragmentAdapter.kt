package com.rungersubb.presentation.ui.adapter.common

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.rungersubb.presentation.ui.fragment.common.viewpager.ViewPagerFragment

class ViewPagerFragmentAdapter<VIEW_MODEL: ViewModel>(
    fragmentManager: FragmentManager,
    lifecycle: Lifecycle,
    private val items: ArrayList<ViewPagerFragment<*, VIEW_MODEL>>
): FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int = items.size

    override fun createFragment(position: Int): Fragment =
        items[position]
}