package com.rungersubb.presentation.mvvm.fragment.login.dialog

import androidx.lifecycle.ViewModel
import com.rungersubb.presentation.model.common.validation.ValidatableUserModel
import com.rungersubb.presentation.model.ui.UserDataInputModel
import com.rungersubb.presentation.mvvm.common.DataInputHelper
import org.joda.time.DateTime

class EnterUserDataViewModel:
    ViewModel(),
    DataInputHelper<UserDataInputModel>,
    ValidatableUserModel {
    override var firstName: String = String()
    override var lastName: String = String()
    override var email: String = String()
    override var password: String = String()
    override var birthDate: DateTime? = null

    var repeatPassword: String = String()

    override val dataModel: UserDataInputModel
        get() = UserDataInputModel(
            firstName,
            lastName,
            email,
            password,
            birthDate
        )

    override fun isModelValid(): Boolean =
        super.isModelValid() && isPasswordsAreMatch()

    fun isPasswordsAreMatch(): Boolean =
        password == repeatPassword
}