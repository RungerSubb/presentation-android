package com.rungersubb.presentation.model.ui

import com.rungersubb.presentation.utils.constants.CommonConstants
import com.rungersubb.presentation.utils.enum.AdapterItemAction

data class ProductionItemModel(
    val item: String,
    val index: Int = CommonConstants.EMPTY_INT,
    val action: AdapterItemAction
)