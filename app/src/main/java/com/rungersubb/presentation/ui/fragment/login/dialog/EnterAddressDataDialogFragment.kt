package com.rungersubb.presentation.ui.fragment.login.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.DialogFragmentEnterAddressDataBinding
import com.rungersubb.presentation.model.request.AddressRequest
import com.rungersubb.presentation.mvvm.fragment.login.dialog.EnterAddressDataViewModel
import com.rungersubb.presentation.ui.fragment.common.dialog.BackgroundNotifierDialogFragment
import com.rungersubb.presentation.ui.fragment.common.interfaces.EditTextStyleHelper
import com.rungersubb.presentation.ui.fragment.common.interfaces.ShapeBasedBackgroundHelper
import com.rungersubb.presentation.utils.constants.Arguments
import com.rungersubb.presentation.utils.constants.RequestKey
import com.rungersubb.presentation.utils.extension.*
import com.rungersubb.presentation.utils.tools.keyboard.KeyboardBehaviorHandler
import org.koin.androidx.viewmodel.ext.android.viewModel

class EnterAddressDataDialogFragment private constructor():
    BackgroundNotifierDialogFragment<DialogFragmentEnterAddressDataBinding, EnterAddressDataViewModel>(),
    KeyboardBehaviorHandler,
    ShapeBasedBackgroundHelper,
    EditTextStyleHelper {


    companion object {
        private const val TAG = "DIALOG_FRAGMENT_ENTER_ADDRESS_DATA"

        private var instance: EnterAddressDataDialogFragment? = null

        val isDisplaying: Boolean
            get() = instance != null


        private val isAbleToDisplay: Boolean
            get() = !isDisplaying && !EnterUserDataDialogFragment.isDisplaying && !CreateOrSelectOrganizationDialogFragment.isDisplaying

        fun display(fragmentManager: FragmentManager, arguments: Bundle = bundleOf()) {
            if (isAbleToDisplay) {
                instance = EnterAddressDataDialogFragment().apply {
                    this.arguments = arguments
                    show(fragmentManager, TAG)
                }
            }
        }
    }

    override val viewModel: EnterAddressDataViewModel by viewModel()
    override val requestKey: String
        get() = RequestKey.ENTER_ADDRESS_DATA_DIALOG_FRAGMENT
    override val animationStyle: Int
        get() = R.style.SlideDialogFragmentAnimations

    //region BackgroundNotifierDialogFragment
    override fun createBinding(inflater: LayoutInflater): DialogFragmentEnterAddressDataBinding =
        DialogFragmentEnterAddressDataBinding.inflate(inflater)

    override fun getResultArguments(): Bundle =
        super.getResultArguments().apply {
            putSerializable(Arguments.ADDRESS_INPUT_DATA, viewModel.dataModel)
        }
    //endregion

    //region Lifecycle
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding?.apply {
            //region ui setup
            registerKeyboardBehaviorHandler(requireActivity())
            createBackButtonBackground(backButtonBackgroundImageView)
            rootConstraintLayout.setOnClickListener { hideKeyboard() }
            backgroundCardView.setOnClickListener { hideKeyboard() }
            contentConstraintLayout.setOnClickListener{ hideKeyboard() }
            closeDialogButton.setOnClickListener { dismiss() }

            confirmButton.disable()
            //endregion

            //region arguments setup
            (requireArguments().getSerializable(Arguments.ADDRESS_INPUT_DATA) as AddressRequest?)?.apply {
                viewModel.country = country
                viewModel.region = region
                viewModel.city = city
                viewModel.street = street
                viewModel.building = building
                viewModel.office = office
                viewModel.zipCode = zipCode

                countryEditText.text = country.toEditable()
                regionEditText.text = region.toEditable()
                cityEditText.text = city.toEditable()
                streetEditText.text = street.toEditable()
                buildingEditText.text = building.toEditable()
                officeEditText.text = office.toEditable()
                zipCodeEditText.text = zipCode.toEditable()

                if(viewModel.isModelValid()) confirmButton.enable()
            }
            //endregion

            //region content behavior
            countryEditText.setOnFocusChangeListener { _, isFocused ->
                if(!isFocused) {
                    with(viewModel) {
                        country = countryEditText.text.toString()

                        if(!isCountryValid() && country.isNotEmpty())
                            enableEditTextErrorStyle(countryEditText)

                        if(isModelValid()) confirmButton.enable()
                        else confirmButton.disable()
                    }
                } else enableEditTextDefaultStyle(countryEditText)
            }

            regionEditText.setOnFocusChangeListener { _, isFocused ->
                if(isFocused) viewModel.region = regionEditText.text.toString()
            }

            cityEditText.setOnFocusChangeListener { _, isFocused ->
                if(!isFocused){
                    with(viewModel){
                        city = cityEditText.text.toString()

                        if(!isCityValid() && city.isNotEmpty())
                            enableEditTextErrorStyle(countryEditText)

                        if(isModelValid()) confirmButton.enable()
                        else confirmButton.disable()
                    }
                } else enableEditTextDefaultStyle(cityEditText)
            }

            streetEditText.setOnFocusChangeListener { _, isFocused ->
                if(!isFocused){
                    with(viewModel){
                        street = streetEditText.text.toString()

                        if(!isStreetValid() && street.isNotEmpty())
                            enableEditTextErrorStyle(streetEditText)

                        if(isModelValid()) confirmButton.enable()
                        else confirmButton.disable()
                    }
                } else enableEditTextDefaultStyle(streetEditText)
            }

            buildingEditText.setOnFocusChangeListener { _, isFocused ->
                if(!isFocused){
                    with(viewModel){
                        building = buildingEditText.text.toString()

                        if(!isBuildingValid() && building.isNotEmpty())
                            enableEditTextErrorStyle(buildingEditText)

                        if(isModelValid()) confirmButton.enable()
                        else confirmButton.disable()
                    }
                } else enableEditTextDefaultStyle(buildingEditText)
            }

            officeEditText.setOnFocusChangeListener{ _, isFocused ->
                if(!isFocused){
                    with(viewModel){
                        office = officeEditText.text.toString()

                        if(!isOfficeValid() && office.isNotEmpty())
                            enableEditTextErrorStyle(officeEditText)

                        if(isModelValid()) confirmButton.enable()
                        else confirmButton.disable()
                    }
                } else enableEditTextDefaultStyle(officeEditText)
            }

            zipCodeEditText.setOnFocusChangeListener { _, isFocused ->
                if(!isFocused){
                    with(viewModel){
                        zipCode = zipCodeEditText.text.toString()

                        if(!isZipCodeValid() && zipCode.isNotEmpty())
                            enableEditTextErrorStyle(zipCodeEditText)

                        if(isModelValid()) confirmButton.enable()
                        else confirmButton.disable()
                    }
                } else enableEditTextDefaultStyle(zipCodeEditText)
            }

            confirmButton.setOnClickListener {
                isDataShouldBeSaved = true
                dismiss()
            }
            //endregion
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        instance = null
    }
    //endregion

    //region KeyboardBehaviorHandler
    override fun onShowKeyboard(keyboardHeight: Int) {
        binding?.confirmButton?.hide()
    }

    override fun onHideKeyboard() {
        binding?.confirmButton?.show()
    }
    //endregion
}