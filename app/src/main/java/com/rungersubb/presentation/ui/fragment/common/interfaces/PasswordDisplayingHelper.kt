package com.rungersubb.presentation.ui.fragment.common.interfaces

import android.text.method.PasswordTransformationMethod
import android.widget.EditText
import android.widget.ImageView
import com.rungersubb.presentation.R

interface PasswordDisplayingHelper {

    fun passwordDisplayingSetup(button: ImageView, passwordField: EditText){
        var isPasswordVisible = false

        button.setImageResource(R.drawable.icon_eye_opened)
        passwordField.transformationMethod = PasswordTransformationMethod.getInstance()

        button.setOnClickListener {
            if(isPasswordVisible){
                button.setImageResource(R.drawable.icon_eye_opened)
                passwordField.transformationMethod = PasswordTransformationMethod.getInstance()
            } else {
                button.setImageResource(R.drawable.icon_eye_closed)
                passwordField.transformationMethod = null
            }
            with(passwordField){ setSelection(text?.toString()?.length ?: 0) }
            isPasswordVisible = !isPasswordVisible
        }

    }
}