package com.rungersubb.presentation.ui.fragment.common.interfaces

import android.animation.ValueAnimator
import android.view.View
import androidx.core.animation.doOnEnd
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import com.rungersubb.presentation.utils.constants.AnimationExtra
import com.rungersubb.presentation.utils.constants.Arguments
import com.rungersubb.presentation.utils.extension.hide
import com.rungersubb.presentation.utils.extension.show

interface DialogFragmentBackgroundHelper {

    interface Notifier {
        fun notifyParentForDialog(parentFragment: Fragment?, requestKey: String, isNeedToShow: Boolean) {
            parentFragment?.setFragmentResult(
                requestKey,
                bundleOf(Arguments.IS_NEED_TO_SHOW_DIALOG_FRAGMENT_BACKGROUND to isNeedToShow)
            )
        }
    }

    fun showDialogFragmentBackground(background: View): Unit =
        with(background) {
            ValueAnimator.ofFloat(
                AnimationExtra.ALPHA_INVISIBLE,
                AnimationExtra.ALPHA_VISIBLE
            ).apply {
                duration = AnimationExtra.DURATION_LONG
                addUpdateListener { animator -> alpha = animator.animatedValue as Float }
            }.start()
        }

    fun hideDialogFragmentBackground(background: View): Unit =
        with(background) {
            ValueAnimator.ofFloat(
                AnimationExtra.ALPHA_VISIBLE,
                AnimationExtra.ALPHA_INVISIBLE
            ).apply {
                duration = AnimationExtra.DURATION_LONG
                addUpdateListener { animator -> alpha = animator.animatedValue as Float }
            }.start()
        }
}