package com.rungersubb.presentation.utils.extension

import android.app.Activity
import android.graphics.Rect
import android.os.IBinder
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.rungersubb.presentation.R

val Activity.statusBarHeight: Int
    get() = Rect().apply { window.decorView.getWindowVisibleDisplayFrame(this) }.top

@Suppress("DEPRECATION")
val Activity.screenHeight: Int
    get() {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }

@Suppress("DEPRECATION")
val Activity.screenWidth: Int
    get() {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.widthPixels
    }

val Activity.rootView: View
    get() = findViewById(R.id.activityRootLinearLayout)

val Activity.inputMethodManager: InputMethodManager
    get() = (getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)


fun Activity.showKeyboard(view: View) {
    view.requestFocus()
    inputMethodManager.showSoftInput(view, 100)
}
fun Activity.hideKeyboard(token: IBinder? = (currentFocus ?: View(this)).windowToken) {
    if (token == null) return
    else {
        inputMethodManager.hideSoftInputFromWindow(token, 0)
        currentFocus?.clearFocus()
    }
}