package com.rungersubb.presentation.utils.extension

import android.util.Log
import com.google.gson.JsonSyntaxException

fun <T> safe(action: () -> T): T? {
    try { return action() }
    catch (exception: Exception){ Log.i("SAVED FROM", "Exception") }

    return null
}

fun <T> safeFromIndexOutOfBound(action: () -> T): T? {
    try{ return action() }
    catch (exception: IndexOutOfBoundsException){ Log.i("SAVED FROM", "IndexOutOfBoundsException") }

    return null
}

fun <T> safeFromIllegalArgument(action: () -> T): T? {
    try { return action() }
    catch (exception: IllegalArgumentException){ Log.i("SAVED FROM", "IllegalArgumentException") }

    return null
}

fun <T> safeFromIllegalState(action: () -> T): T? {
    try { return action() }
    catch (exception: IllegalStateException){ Log.i("SAVED FROM", "IllegalStateException") }

    return null
}

fun <T> safeFromJsonSyntax(action: () -> T): T? {
    try { return action() }
    catch (exception: JsonSyntaxException){ Log.i("SAVED FROM", "JsonSyntaxException") }

    return null
}