package com.rungersubb.presentation.ui.fragment.login.viewpager

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.lifecycleScope
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.VpFragmentSelectOrganizationBinding
import com.rungersubb.presentation.model.response.CompactOrganizationResponse
import com.rungersubb.presentation.mvvm.fragment.login.dialog.CreateOrSelectOrganizationViewModel
import com.rungersubb.presentation.ui.adapter.login.SelectOrganizationAdapter
import com.rungersubb.presentation.ui.fragment.common.viewpager.ViewPagerFragment
import com.rungersubb.presentation.ui.fragment.common.interfaces.AdapterHelper
import com.rungersubb.presentation.ui.fragment.common.interfaces.UiUpdater
import com.rungersubb.presentation.utils.constants.Arguments
import com.rungersubb.presentation.utils.constants.CommonConstants
import com.rungersubb.presentation.utils.constants.RequestKey
import com.rungersubb.presentation.utils.constants.ViewPagerPageNumber
import com.rungersubb.presentation.utils.extension.launchWhenStarted
import com.rungersubb.presentation.utils.extension.safeFromIllegalState
import kotlinx.coroutines.flow.onEach
import kotlin.concurrent.thread

class SelectOrganizationFragment(viewModel: CreateOrSelectOrganizationViewModel) :
    ViewPagerFragment<VpFragmentSelectOrganizationBinding, CreateOrSelectOrganizationViewModel>(viewModel),
    AdapterHelper<SelectOrganizationAdapter, CompactOrganizationResponse?>,
    UiUpdater {

    override val adapter: SelectOrganizationAdapter by lazy {
        SelectOrganizationAdapter(arrayListOf()) { index, item -> onAdapterItemClicked(index, item)}
    }

    //region ViewPagerFragment
    override fun createBinding(inflater: LayoutInflater): VpFragmentSelectOrganizationBinding =
        VpFragmentSelectOrganizationBinding.inflate(inflater)

    override fun setViewPagerCurrentPage() {
        viewModel.selectedViewPagerPage = ViewPagerPageNumber.SELECT_ORGANIZATION
    }
    //endregion

    //region Lifecycle
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(viewModel){
            uuidFlow.onEach { data ->
                if(data?.isNotEmpty() == true)
                    adapter.selectItem(
                        CompactOrganizationResponse(data,
                            selectedNameFlow.value ?: adapter.getItemByUuid(data)?.name ?: String()
                        )
                    )
            }.launchWhenStarted(lifecycleScope)

            selectedNameFlow.onEach { data ->
                if(data != null)
                    binding?.selectedOrganizationTextView?.text = "\"${data}\" ${getString(R.string.selected)}"

            }.launchWhenStarted(lifecycleScope)

            listFlow.onEach { data ->
                val isNeedToClearOldData = data.first
                val list = data.second

                if(isNeedToClearOldData) adapter.clear()

                with(adapter) {
                    removeLastIfItNull()
                    add(list)
                }
            }.launchWhenStarted(lifecycleScope)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding?.apply {
            selectOrganizationRecyclerView.apply {
                val manager = layoutManager(requireContext())
                adapter = this@SelectOrganizationFragment.adapter
                layoutManager = manager

                setOnScrollChangeListener(onLastElementDisplayedListener(manager){
                    viewModel.requestListNextPage(searchEditText.text.toString())
                })

                setOnTouchListener { _, _ ->
                    setFragmentResult(
                        RequestKey.CREATE_OR_SELECT_ORGANIZATION_VIEW_PAGER_FRAGMENT,
                        bundleOf(Arguments.IS_NEED_TO_HIDE_KEYBOARD to true)
                    )
                    false
                }
            }

            searchEditText.addTextChangedListener{ text ->
                viewModel.apply {
                    clear()
                    requestListNextPage(text?.toString() ?: String())
                }
            }

            viewModel.requestListNextPage(searchEditText.text.toString())

        }
    }

    override fun onResume() {
        super.onResume()
        with(viewModel) {
            adapter.selectedItem()?.apply {
                setUuid(uuid)
                setSelectedName(name)
            }
        }

        setFragmentResult(
            RequestKey.CREATE_OR_SELECT_ORGANIZATION_VIEW_PAGER_FRAGMENT,
            bundleOf(Arguments.UPDATE_UI to true)
        )
    }
    //endregion

    //region AdapterHelper
    @SuppressLint("SetTextI18n")
    override fun onAdapterItemClicked(index: Int, item: CompactOrganizationResponse?) {
        with(viewModel) {
            disableUi()

            item?.run {
                setSelectedName(name)
                setUuid(uuid)
            }

            //
            thread {
                safeFromIllegalState {
                    Thread.sleep(CommonConstants.CREATE_ORGANIZATION_UI_UPDATE_DELAY)
                    requireActivity().runOnUiThread {
                        safeFromIllegalState {
                           enableUi()
                        }
                    }
                }
            }

        }
    }
    //endregion

    //region UiUpdater
    override fun enableUi() {
        setFragmentResult(
            RequestKey.CREATE_OR_SELECT_ORGANIZATION_VIEW_PAGER_FRAGMENT,
            bundleOf(Arguments.IS_NEED_TO_ENABLE_UI to true)
        )
    }

    override fun disableUi() {
        setFragmentResult(
            RequestKey.CREATE_OR_SELECT_ORGANIZATION_VIEW_PAGER_FRAGMENT,
            bundleOf(Arguments.IS_NEED_TO_DISABLE_UI to true)
        )
    }
    //endregion

}
