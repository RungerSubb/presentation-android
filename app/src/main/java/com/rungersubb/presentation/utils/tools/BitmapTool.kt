package com.rungersubb.presentation.utils.tools

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.rungersubb.presentation.R
import com.rungersubb.presentation.application.PresentationApplication

object BitmapTool {

    private val context: Context
        get() = PresentationApplication.get().applicationContext

    private const val DIMENSION_EMPTY = 0F

    val backButtonBackground: Bitmap? by lazy {
        val leftBitmap = getInvertedBottomRightShapeBitmap(context)
        val topBitmap = getInvertedBottomRightShapeBitmap(context)
        val centerBitmap = ContextCompat.getDrawable(
            context,
            R.drawable.shape_back_button_background
        )?.toBitmap()

        return@lazy mergeBitmapsAsBackButtonBackground(
            topBitmap,
            leftBitmap,
            centerBitmap,
        )
    }

    private fun getInvertedBottomRightShapeBitmap(context: Context): Bitmap? =
        getInvertedBitmap(
            context,
            ContextCompat.getDrawable(
                context,
                R.drawable.shape_quarter_cirlce_bottom_right
            )
        )

    private fun mergeBitmapsAsBackButtonBackground(
        bitmapTop: Bitmap?,
        bitmapLeft: Bitmap?,
        bitmapCenter: Bitmap?
    ): Bitmap? {
        if(bitmapTop == null || bitmapLeft == null || bitmapCenter == null) return null

        val width = bitmapLeft.width + bitmapCenter.width
        val height = bitmapCenter.height + bitmapTop.height

        val smallBitmapMargin = bitmapCenter.width.toFloat()
        val largeBitmapMargin = bitmapLeft.width.toFloat()

        val baseBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(baseBitmap)

        canvas.drawBitmap(bitmapLeft, DIMENSION_EMPTY, smallBitmapMargin, null)
        canvas.drawBitmap(bitmapTop, smallBitmapMargin, DIMENSION_EMPTY, null)
        canvas.drawBitmap(bitmapCenter, largeBitmapMargin, largeBitmapMargin, null)

        return baseBitmap
    }

    private fun getInvertedBitmap(context: Context, drawable: Drawable?) : Bitmap? {
        if(drawable == null) return null

        val baseBitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(baseBitmap)
        drawable.setBounds( 0, 0, canvas.width, canvas.height)

        val drawable2 = ContextCompat.getDrawable(context, R.drawable.shape_square)!!.apply {
            setBounds( 0, 0, canvas.width, canvas.height)
        }

        drawable.draw(canvas)
        val bitmapFromDrawable = Bitmap.createBitmap(baseBitmap)
        drawable2.draw(canvas)
        val paint = Paint()
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_OUT)
        canvas.drawBitmap(bitmapFromDrawable, 0F, 0F, paint)

        return Bitmap.createBitmap(baseBitmap)
    }
}