package com.rungersubb.presentation.utils.extension

import androidx.lifecycle.LifecycleCoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect

@Suppress("LABEL_RESOLVE_WILL_CHANGE")
fun <T> Flow<T>.launchWhenStarted(lifecycleScope: LifecycleCoroutineScope){
    lifecycleScope.launchWhenStarted {
        this@launchWhenStarted.collect()
    }
}

@Suppress("LABEL_RESOLVE_WILL_CHANGE")
fun <T> Flow<T>.launchWhenResumed(lifecycleScope: LifecycleCoroutineScope){
    lifecycleScope.launchWhenResumed {
        this@launchWhenResumed.collect()
    }
}