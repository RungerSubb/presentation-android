package com.rungersubb.presentation.model.request

import com.google.gson.annotations.SerializedName
import com.rungersubb.presentation.model.common.validation.ValidatableOrganizationModel
import java.io.Serializable

data class OrganizationRequest(
    @SerializedName("uuid")
    override val uuid: String?,

    @SerializedName("name")
    override val name: String?,

    @SerializedName("production")
    override val production: ArrayList<String>
): ValidatableOrganizationModel, Serializable