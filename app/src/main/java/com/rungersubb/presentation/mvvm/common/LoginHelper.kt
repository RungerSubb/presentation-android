package com.rungersubb.presentation.mvvm.common

interface LoginHelper {
    fun loadSavedInputs()
    fun clearData()

    fun isInputCompleted(): Boolean
    fun isNoDataEntered(): Boolean
}