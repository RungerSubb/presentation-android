package com.rungersubb.presentation.ui.adapter.login

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.HolderSignUpStepBinding
import com.rungersubb.presentation.model.enums.SignUpStepCondition
import com.rungersubb.presentation.model.ui.SignUpStepModel
import com.rungersubb.presentation.ui.adapter.common.BasicAdapter
import com.rungersubb.presentation.ui.adapter.common.DefaultActionAdapter
import com.rungersubb.presentation.utils.extension.disable
import com.rungersubb.presentation.utils.extension.enable

class SignUpStepsAdapter(
    items: ArrayList<SignUpStepModel> = arrayListOf(),
    action: (index: Int, item: SignUpStepModel) -> Unit
): DefaultActionAdapter<SignUpStepModel, SignUpStepsAdapter.SignUpStepViewHolder>(items, action) {

    companion object{
        const val ENTER_USER_DATA_INDEX = 0
        const val ENTER_ORGANIZATION_DATA_INDEX = 1
        const val ENTER_ADDRESS_DATA_INDEX = 2
    }

    class SignUpStepViewHolder(binding: HolderSignUpStepBinding):
        BasicAdapter.BasicViewHolder<SignUpStepModel, HolderSignUpStepBinding>(binding){

        override fun display(data: SignUpStepModel): Unit =
            with(binding){
                actionButton.text = getString(data.stringResource)

                when(data.condition){
                    SignUpStepCondition.ACCEPTED -> {
                        statusImageView.apply {
                            setImageResource(R.drawable.icon_accepted)
                            setColorFilter(getColor(R.color.dim_green))
                            enable()
                        }
                        actionButton.apply {
                            setCardBackgroundColor(getColor(R.color.dark_dim_blue))
                            enable()
                        }
                    }
                    SignUpStepCondition.DENIED -> {
                        statusImageView.apply {
                            setImageResource(R.drawable.icon_denied)
                            setColorFilter(getColor(R.color.grey))
                            enable()
                        }
                        actionButton.apply {
                            setCardBackgroundColor(getColor(R.color.dark_dim_blue))
                            enable()
                        }
                    }
                    SignUpStepCondition.DISABLED -> {
                        statusImageView.apply {
                            setImageResource(R.drawable.icon_blocked)
                            setColorFilter(getColor(R.color.grey))
                            disable()
                        }
                        actionButton.apply {
                            setCardBackgroundColor(getColor(R.color.grey))
                            disable()
                        }
                    }
                }
            }

        override fun setOnClickListener(listener: View.OnClickListener?): Unit =
            binding.actionButton.setOnClickListener(listener)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SignUpStepViewHolder =
        SignUpStepViewHolder(HolderSignUpStepBinding.inflate(LayoutInflater.from(parent.context)))
}