package com.rungersubb.presentation.ui.fragment.login.screen

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.FragmentSignInBinding
import com.rungersubb.presentation.mvvm.fragment.login.screens.SignInViewModel
import com.rungersubb.presentation.ui.fragment.common.interfaces.AlertDialogDisplayer
import com.rungersubb.presentation.ui.fragment.common.interfaces.EditTextStyleHelper
import com.rungersubb.presentation.ui.fragment.common.main.NavigationFragment
import com.rungersubb.presentation.ui.fragment.common.interfaces.PasswordDisplayingHelper
import com.rungersubb.presentation.ui.fragment.common.interfaces.ViewAnimationHelper
import com.rungersubb.presentation.utils.constants.AnimationExtra
import com.rungersubb.presentation.utils.constants.Arguments
import com.rungersubb.presentation.utils.constants.CommonConstants
import com.rungersubb.presentation.utils.extension.*
import com.rungersubb.presentation.utils.tools.keyboard.KeyboardBehaviorHandler
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignInFragment :
    NavigationFragment<FragmentSignInBinding, SignInViewModel>(),
    ViewAnimationHelper<FragmentSignInBinding>,
    KeyboardBehaviorHandler,
    PasswordDisplayingHelper,
    EditTextStyleHelper,
    AlertDialogDisplayer {

    override val viewModel: SignInViewModel by viewModel()

    //region NavigationFragment
    override fun createBinding(inflater: LayoutInflater): FragmentSignInBinding =
        FragmentSignInBinding.inflate(inflater)
    //endregion

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(viewModel){
            emailFlow.onEach {
                binding?.apply {
                    if(!isEmailValid() && email.isNotEmpty())
                        enableEditTextErrorStyle(emailEditText)

                    if(isModelValid()) signInButton.enable()
                    else signInButton.disable()

                    if(signInButton.isVisible) { //that's hidden when keyboard is visible
                        if (isNoDataEntered()) clearInputTextView.hide()
                        else clearInputTextView.show()
                    }
                }
            }.launchWhenStarted(lifecycleScope)

            passwordFlow.onEach {
                binding?.apply {
                    if(!isPasswordValid() && password.isNotEmpty())
                        enableEditTextErrorStyle(passwordEditText)

                    if(isModelValid()) signInButton.enable()
                    else signInButton.disable()


                    if(signInButton.isVisible) { //that's hidden when keyboard is visible
                        if (isNoDataEntered()) clearInputTextView.hide()
                        else clearInputTextView.show()
                    }
                }
            }.launchWhenStarted(lifecycleScope)

            authenticationFlow.onEach { data ->
                if(!AlertDialogDisplayer.isLoaderDialogDisplaying) return@onEach

                if(data?.token?.startsWith(CommonConstants.TOKEN_BEARER) == true && AlertDialogDisplayer.isLoaderDialogDisplaying)
                    Toast.makeText(requireContext(), "AUTHENTICATED", Toast.LENGTH_SHORT).show()
                else if(AlertDialogDisplayer.isLoaderDialogDisplaying)
                    Toast.makeText(requireContext(), "ERROR", Toast.LENGTH_SHORT).show()

                if(AlertDialogDisplayer.isLoaderDialogDisplaying)
                    AlertDialogDisplayer.dismissDialogLoader()
            }.launchWhenStarted(lifecycleScope)

            loadSavedInputs()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        binding?.apply {
            registerKeyboardBehaviorHandler(requireActivity())
            passwordDisplayingSetup(showHidePasswordButtonImageView, passwordEditText)
            rootConstraintLayout.setOnClickListener { requireActivity().hideKeyboard() }

            if (requireArguments().getBoolean(Arguments.ENABLE_VIEW_ANIMATION)) {
                getValueAnimator(this).start()
                arguments = bundleOf()
            }

            //region content behavior
            signInButton.apply {
                setOnClickListener {
                    displayLoaderDialog(requireActivity())
                    viewModel.authenticate()
                }
                if(!viewModel.isInputCompleted()) disable()
            }

            registerButton.setOnClickListener {
                navigate(R.id.action_signInFragment_to_signUpFragment)
            }

            googleButtonBinding.rootCardView.setOnClickListener {

            }

            emailEditText.apply {
                text = viewModel.email.toEditable()

                setOnFocusChangeListener { _, isFocused ->
                    if(!isFocused) viewModel.setEmail(text.toString())
                    else enableEditTextDefaultStyle(this)
                }
            }

            passwordEditText.apply {
                text = viewModel.password.toEditable()

                setOnFocusChangeListener { _, isFocused ->
                    if(!isFocused) viewModel.setPassword(text.toString())
                    else enableEditTextDefaultStyle(this)
                }
            }

            clearInputTextView.apply {
                if (viewModel.isNoDataEntered()) hide()

                setOnTouchListener { _, motionEvent ->
                    when (motionEvent.action) {
                        MotionEvent.ACTION_DOWN -> {
                            typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
                            text = SpannableStringBuilder(text.toString()).apply {
                                setSpan(UnderlineSpan(), 0, text.length, 0)
                            }
                        }
                        MotionEvent.ACTION_UP -> {
                            typeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL)
                            text = getString(R.string.Clear)
                        }
                    }

                    false
                }

                setOnClickListener {
                    displayYesOrNoDialog(
                        requireActivity(),
                        getString(R.string.Clear_input_label),
                        getString(R.string.Clear_input_message),
                        {
                            emailEditText.apply {
                                text = String().toEditable()
                                enableEditTextDefaultStyle(this)
                            }
                            passwordEditText.apply {
                                text = String().toEditable()
                                enableEditTextDefaultStyle(this)
                            }
                            viewModel.clearData()
                        }
                    )
                }
            }
        }
        //endregion
    }
    //endregion

    //region ViewAnimationHelper
    override fun getValueAnimator(binding: FragmentSignInBinding): ValueAnimator =
        with(binding){
            return@with ValueAnimator.ofFloat(
                AnimationExtra.ALPHA_INVISIBLE,
                AnimationExtra.ALPHA_VISIBLE
            ).apply {
                duration = AnimationExtra.DURATION_FROM_SPLASH_TO_SIGN_IN
                addUpdateListener { animator ->
                    val animatedValue = animator.animatedValue as Float

                    googleButtonBinding.rootCardView.alpha = animatedValue
                    orTextView.alpha = animatedValue
                    emailEditText.alpha = animatedValue
                    passwordEditText.alpha = animatedValue
                    signInButton.alpha = animatedValue
                    registerButton.alpha = animatedValue
                }
            }
        }
    //endregion

    //region KeyboardBehaviorHandler
    override fun onShowKeyboard(keyboardHeight: Int) {
        binding?.apply {
            hideSeveralViews(
                googleButtonBinding.rootCardView,
                orTextView,
                signInButton,
                registerButton,
                clearInputTextView
            )
        }
    }

    override fun onHideKeyboard() {
        binding?.apply {
            showSeveralViews(
                googleButtonBinding.rootCardView,
                orTextView,
                signInButton,
                registerButton
            )
            if(!viewModel.isNoDataEntered()) clearInputTextView.show()
            requireActivity().currentFocus?.clearFocus()
        }
    }
    //endregion
}