package com.rungersubb.presentation.utils.extension

import android.app.Activity
import android.os.IBinder
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager

val Window.inputMethodManager: InputMethodManager
    get() = (context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager)

fun Window.hideKeyboard(token: IBinder? = (currentFocus ?: View(this.context)).windowToken){
    if (token == null) return
    else {
        inputMethodManager.hideSoftInputFromWindow(token, 0)
        currentFocus?.clearFocus()
    }
}