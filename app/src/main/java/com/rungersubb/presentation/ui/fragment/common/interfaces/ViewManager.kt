package com.rungersubb.presentation.ui.fragment.common.interfaces

import android.view.View
import android.view.animation.Animation
import com.rungersubb.presentation.R
import com.rungersubb.presentation.utils.extension.*

interface ViewManager {

    fun showSeveralViews(vararg views: View): Unit =
        views.forEach { view -> view.show() }

    fun hideSeveralViews(vararg views: View): Unit =
        views.forEach { view -> view.hide() }

    fun concealSeveralViews(vararg views: View): Unit =
        views.forEach { view -> view.conceal() }

    fun enableSeveralViews(vararg views: View): Unit =
        views.forEach { view -> view.enable() }

    fun disableSeveralViews(vararg views: View): Unit =
        views.forEach { view -> view.disable() }
}