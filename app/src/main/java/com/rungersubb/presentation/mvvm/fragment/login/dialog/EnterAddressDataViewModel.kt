package com.rungersubb.presentation.mvvm.fragment.login.dialog

import androidx.lifecycle.ViewModel
import com.rungersubb.presentation.model.common.validation.ValidatableAddressModel
import com.rungersubb.presentation.model.request.AddressRequest
import com.rungersubb.presentation.mvvm.common.DataInputHelper

class EnterAddressDataViewModel:
    ViewModel(),
    DataInputHelper<AddressRequest>,
    ValidatableAddressModel {

    override var country: String = String()
    override var region: String = String()
    override var city: String = String()
    override var street: String = String()
    override var building: String = String()
    override var office: String = String()
    override var zipCode: String = String()

    override val dataModel: AddressRequest
        get() = AddressRequest(country, region, city, street, building, office, zipCode)
}