package com.rungersubb.presentation.ui.fragment.login.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.DialogFragmentEnterUserDataBinding
import com.rungersubb.presentation.model.ui.UserDataInputModel
import com.rungersubb.presentation.mvvm.fragment.login.dialog.EnterUserDataViewModel
import com.rungersubb.presentation.ui.fragment.common.dialog.BackgroundNotifierDialogFragment
import com.rungersubb.presentation.ui.fragment.common.interfaces.ShapeBasedBackgroundHelper
import com.rungersubb.presentation.ui.fragment.common.interfaces.AlertDialogDisplayer
import com.rungersubb.presentation.ui.fragment.common.interfaces.EditTextStyleHelper
import com.rungersubb.presentation.ui.fragment.common.interfaces.PasswordDisplayingHelper
import com.rungersubb.presentation.utils.constants.Arguments
import com.rungersubb.presentation.utils.constants.RequestKey
import com.rungersubb.presentation.utils.extension.*
import com.rungersubb.presentation.utils.tools.keyboard.KeyboardBehaviorHandler
import org.koin.androidx.viewmodel.ext.android.viewModel

class EnterUserDataDialogFragment private constructor():
    BackgroundNotifierDialogFragment<DialogFragmentEnterUserDataBinding, EnterUserDataViewModel>(),
    KeyboardBehaviorHandler,
    PasswordDisplayingHelper,
    ShapeBasedBackgroundHelper,
    AlertDialogDisplayer,
    EditTextStyleHelper {

    companion object {
        private const val TAG = "DIALOG_FRAGMENT_ENTER_USER_DATA"

        private var instance: EnterUserDataDialogFragment? = null

        val isDisplaying: Boolean
            get() = instance != null

        private val isAbleToDisplay: Boolean
            get() = !isDisplaying && !CreateOrSelectOrganizationDialogFragment.isDisplaying && !EnterAddressDataDialogFragment.isDisplaying

        fun display(fragmentManager: FragmentManager, arguments: Bundle = bundleOf()) {
            if (isAbleToDisplay) {
                instance = EnterUserDataDialogFragment().apply {
                    this.arguments = arguments
                    show(fragmentManager, TAG)
                }
            }
        }
    }

    override val viewModel: EnterUserDataViewModel by viewModel()
    override val requestKey: String
        get() = RequestKey.ENTER_USER_DATA_DIALOG_FRAGMENT
    override val animationStyle: Int
        get() = R.style.SlideDialogFragmentAnimations

    //region BackgroundNotifierDialogFragment
    override fun createBinding(inflater: LayoutInflater): DialogFragmentEnterUserDataBinding =
        DialogFragmentEnterUserDataBinding.inflate(inflater)

    override fun getResultArguments(): Bundle =
        super.getResultArguments().apply {
            putSerializable(Arguments.USER_INPUT_DATA, viewModel.dataModel)
        }
    //endregion

    //region Lifecycle
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding?.apply {
            //region ui setup
            registerKeyboardBehaviorHandler(requireActivity())
            createBackButtonBackground(backButtonBackgroundImageView)
            passwordDisplayingSetup(showHidePasswordButtonImageView, passwordEditText)
            passwordDisplayingSetup(showHideRepeatPasswordButtonImageView, repeatPasswordEditText)
            rootConstraintLayout.setOnClickListener { hideKeyboard() }
            backgroundCardView.setOnClickListener { hideKeyboard() }
            contentConstraintLayout.setOnClickListener{ hideKeyboard() }
            closeDialogButton.setOnClickListener { dismiss() }

            confirmButton.disable()
            //endregion

            //region arguments setup
            (requireArguments().getSerializable(Arguments.USER_INPUT_DATA) as UserDataInputModel?)?.apply{
                viewModel.firstName = firstName
                viewModel.lastName = lastName
                viewModel.email = email
                viewModel.password = password
                viewModel.repeatPassword = password
                viewModel.birthDate = birthDate

                firstNameEditText.text = firstName.toEditable()
                lastNameEditText.text = lastName.toEditable()
                emailEditText.text = email.toEditable()
                passwordEditText.text = password.toEditable()
                repeatPasswordEditText.text = password.toEditable()
                birthDateButton.text = (birthDate?.displayingDateString ?: String()).toEditable()

                if(viewModel.isModelValid())confirmButton.enable()
            }
            //endregion

            //region content behaviour
            firstNameEditText.setOnFocusChangeListener { _, isFocused ->
                if(!isFocused) {
                    with(viewModel) {
                        firstName = firstNameEditText.text.toString()

                        if(!isFirstNameValid() && firstName.isNotEmpty())
                            enableEditTextErrorStyle(firstNameEditText)

                        if(isModelValid()) confirmButton.enable()
                        else confirmButton.disable()
                    }
                } else enableEditTextDefaultStyle(firstNameEditText)
            }

            lastNameEditText.setOnFocusChangeListener { _, isFocused ->
                if(!isFocused) {
                    with(viewModel) {
                        lastName = lastNameEditText.text.toString()

                        if(!isLastNameValid() && lastName.isNotEmpty())
                            enableEditTextErrorStyle(lastNameEditText)

                        if(isModelValid()) confirmButton.enable()
                        else confirmButton.disable()
                    }
                } else enableEditTextDefaultStyle(lastNameEditText)
            }

            emailEditText.setOnFocusChangeListener { _, isFocused ->
                if(!isFocused) {
                    with(viewModel) {
                        email = emailEditText.text.toString()

                        if(!isEmailValid() && email.isNotEmpty()) enableEditTextErrorStyle(emailEditText)

                        if(isModelValid()) confirmButton.enable()
                        else confirmButton.disable()
                    }
                } else enableEditTextDefaultStyle(emailEditText)
            }

            passwordEditText.setOnFocusChangeListener { _, isFocused ->
                if (!isFocused) {
                    with(viewModel) {
                        password = passwordEditText.text.toString()

                        if(!isPasswordValid() && password.isNotEmpty()){
                            enableEditTextErrorStyle(passwordEditText)
                            enableEditTextDefaultStyle(repeatPasswordEditText)
                        } else if(!isPasswordsAreMatch() && repeatPassword.isNotEmpty() && password.isNotEmpty())
                            enableEditTextErrorStyle(passwordEditText, repeatPasswordEditText)
                        else enableEditTextDefaultStyle(repeatPasswordEditText)


                        if (isModelValid()) confirmButton.enable()
                        else confirmButton.disable()
                    }
                } else enableEditTextDefaultStyle(passwordEditText)
            }

            repeatPasswordEditText.setOnFocusChangeListener { _, isFocused ->
                if(!isFocused) with(viewModel) {
                    repeatPassword = repeatPasswordEditText.text.toString()

                    if(!isPasswordsAreMatch() && repeatPassword.isNotEmpty())
                        if(isPasswordValid()) enableEditTextErrorStyle(passwordEditText, repeatPasswordEditText)
                        else if(password.isNotEmpty()) enableEditTextErrorStyle(passwordEditText)
                    else if(!isPasswordValid()) enableEditTextErrorStyle(passwordEditText)

                    if(isPasswordValid() && isPasswordsAreMatch())
                        enableEditTextDefaultStyle(passwordEditText)

                    if (isModelValid()) confirmButton.enable()
                    else confirmButton.disable()

                } else enableEditTextDefaultStyle(repeatPasswordEditText)
            }

            birthDateButton.setOnClickListener {
                displayDatePickerDialog(
                    requireActivity(),
                    viewModel.birthDate,
                    { result ->
                        with(viewModel) {
                            birthDate = result

                            birthDateButton.apply {
                                text = result.displayingDateString
                                if(isBirthDateValid()) {
                                    strokeWidth = getDimension(R.dimen.dimen_empty).toInt()
                                    setTextColor(getColor(R.color.black))
                                }
                                else {
                                    strokeWidth = getDimension(R.dimen.stroke_2dp).toInt()
                                    setTextColor(getColor(R.color.dim_red))
                                }
                            }

                            if (isModelValid()) confirmButton.enable()
                            else confirmButton.disable()
                        }
                    }
                )
            }

            confirmButton.setOnClickListener {
                isDataShouldBeSaved = true
                dismiss()
            }
            //endregion
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        instance = null
    }
    //endregion

    //region KeyboardBehaviorHandler
    override fun onShowKeyboard(keyboardHeight: Int) {
        binding?.apply {
            hideSeveralViews(confirmButton, birthDateButton, calendarImageView)
        }
    }

    override fun onHideKeyboard() {
        binding?.apply {
            showSeveralViews(confirmButton, birthDateButton, calendarImageView)
            dialog?.currentFocus?.clearFocus()
        }
    }
    //endregion
}
