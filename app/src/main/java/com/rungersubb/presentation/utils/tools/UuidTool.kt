package com.rungersubb.presentation.utils.tools

import com.rungersubb.presentation.utils.extension.safeFromIllegalArgument
import java.util.*

object UuidTool {

    fun isStringValid(source: String): Boolean =
        safeFromIllegalArgument { UUID.fromString(source) != null } ?: false

}