package com.rungersubb.presentation.model.common.validation

import com.rungersubb.presentation.utils.constants.RegexValues

interface ValidatableAddressModel: ValidatableModel {
    val country: String
    val region: String
    val city: String
    val street: String
    val building: String
    val office: String
    val zipCode: String

    fun isCountryValid(): Boolean =
        country.isNotEmpty() && country.isNotBlank()

    fun isCityValid(): Boolean =
        city.isNotEmpty() && city.isNotBlank()

    fun isStreetValid(): Boolean =
        street.isNotEmpty() && street.isNotBlank()

    fun isBuildingValid(): Boolean =
        building.isNotEmpty() && building.isNotBlank() && building.matches(Regex(RegexValues.DIGITS))

    fun isOfficeValid(): Boolean =
        office.isNotEmpty() && office.isNotBlank()

    fun isZipCodeValid(): Boolean =
        zipCode.isNotEmpty() && zipCode.isNotBlank()

    override fun isModelValid(): Boolean =
        isCountryValid() && isCityValid() && isStreetValid() && isBuildingValid() && isOfficeValid() && isZipCodeValid()
}