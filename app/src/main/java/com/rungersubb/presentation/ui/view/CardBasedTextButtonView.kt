package com.rungersubb.presentation.ui.view

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import androidx.core.content.ContextCompat
import androidx.core.view.updateLayoutParams
import com.google.android.material.card.MaterialCardView
import com.rungersubb.presentation.R
import com.rungersubb.presentation.databinding.ViewCardBasedTextButtonBinding
import com.rungersubb.presentation.ui.view.common.CoreView

class CardBasedTextButtonView(
    context: Context,
    attributeSet: AttributeSet
): MaterialCardView(context, attributeSet), CoreView<ViewCardBasedTextButtonBinding> {

    override val binding: ViewCardBasedTextButtonBinding =
        ViewCardBasedTextButtonBinding.inflate(
            LayoutInflater.from(context),
            this,
            true
        )

    var text: CharSequence
        get() = binding.mainTextView.text
        set(value){ binding.mainTextView.text = value }
    var textSize: Float
        get() = binding.mainTextView.textSize
        set(value) { binding.mainTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, value)}
    var hint: CharSequence
        get() = binding.mainTextView.hint
        set(value) { binding.mainTextView.hint = value }
    var textGravity: Int
        get() = binding.mainTextView.gravity
        set(value) { binding.mainTextView.gravity = value }
    var typeface: Typeface
        get() = binding.mainTextView.typeface
        set(value) { binding.mainTextView.typeface = value }

    init {
        val attributes = context.obtainStyledAttributes(
            attributeSet,
            R.styleable.CardBasedTextButtonView
        )
        setupWithAttributes(attributes)
        attributes.recycle()
    }

    override fun setupWithAttributes(attributes: TypedArray): Unit =
        with(binding){
            attributes.apply {

                mainTextView.apply {
                    text = getString(R.styleable.CardBasedTextButtonView_cbtbv_text) ?: String()
                    hint = getString(R.styleable.CardBasedTextButtonView_cbtbv_hint) ?: String()

                    setHintTextColor(
                        getColor(
                            R.styleable.CardBasedTextButtonView_cbtbv_hintColor,
                            ContextCompat.getColor(context, R.color.white)
                        )
                    )

                    setTextSize(
                        TypedValue.COMPLEX_UNIT_PX,
                        getDimension(
                            R.styleable.CardBasedTextButtonView_cbtbv_textSize,
                            resources.getDimension(R.dimen.text_16sp)
                        )
                    )
                    setTextColor(
                        getColor(
                            R.styleable.CardBasedTextButtonView_cbtbv_textColor,
                            ContextCompat.getColor(context, R.color.white)
                        )
                    )

                    typeface = Typeface.create(
                        Typeface.DEFAULT,
                        getInt(
                            R.styleable.CardBasedTextButtonView_cbtbv_textStyle,
                            Typeface.NORMAL
                        )
                    )

                    gravity = getInt(
                        R.styleable.CardBasedTextButtonView_cbtbv_textGravity,
                        Gravity.CENTER
                    )

                    val horizontalPadding = getDimensionPixelSize(
                        R.styleable.CardBasedTextButtonView_cbtbv_textHorizontalPadding,
                        0
                    )
                    val verticalPadding = getDimensionPixelSize(
                        R.styleable.CardBasedTextButtonView_cbtbv_textVerticalPadding,
                        0
                    )

                    updateLayoutParams<LayoutParams> {
                        setMargins(
                            horizontalPadding,
                            verticalPadding,
                            horizontalPadding,
                            verticalPadding
                        )
                    }
                }
            }
        }

    //region setters
    fun setTextColor(color: Int): Unit =
        binding.mainTextView.setTextColor(color)

    fun setHintColor(color: Int): Unit =
        binding.mainTextView.setHintTextColor(color)
    //endregion
}