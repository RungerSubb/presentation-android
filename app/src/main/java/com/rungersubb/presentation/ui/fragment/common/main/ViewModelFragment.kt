package com.rungersubb.presentation.ui.fragment.common.main

import androidx.lifecycle.ViewModel
import androidx.viewbinding.ViewBinding
import com.rungersubb.presentation.ui.fragment.common.interfaces.ViewManager

abstract class ViewModelFragment<BINDING: ViewBinding, VIEW_MODEL: ViewModel> : BindingFragment<BINDING>(),
    ViewManager {
    protected abstract val viewModel: VIEW_MODEL
}